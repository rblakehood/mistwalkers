package com.zarokima.mistwalkers;

import java.io.File;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import com.zarokima.mistwalkers.explore.ExploreActivity;
import com.zarokima.mistwalkers.scan.ScanActivity;

public class MainMenuActivity extends Activity
{
	private final static String TAG = MainMenuActivity.class.getSimpleName();

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		Log.d(TAG, "Starting...");
		super.onCreate(savedInstanceState);
		// requesting to turn the title OFF
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		// making it full screen
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

		setContentView(R.layout.mainmenu);

		Button b = (Button) findViewById(R.id.btn_newgame);
		b.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				newgameClicked();
			}
		});

		b = (Button) findViewById(R.id.btn_loadgame);
		b.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				loadgameClicked();
			}
		});

		// load game button only enabled if save files exists
		File file = getFileStreamPath(ExploreActivity.SAVE_FILE);
		if (file.exists())
		{
			b.setEnabled(true);
		}
		else
		{
			b.setEnabled(false);
		}

		b = (Button) findViewById(R.id.btn_scan);
		b.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				scanClicked();
			}
		});	
	}

	@Override
	protected void onDestroy()
	{
		Log.d(TAG, "Destroying...");
		super.onDestroy();
	}

	@Override
	protected void onStop()
	{
		Log.d(TAG, "Stopping...");
		super.onStop();
	}

	private void newgameClicked()
	{
		// get player name from user
		AlertDialog.Builder dialog = new AlertDialog.Builder(this);
		dialog.setTitle("Enter your name.");
		final EditText input = new EditText(this);
		dialog.setView(input);
		dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener()
		{
			public void onClick(DialogInterface dialog, int whichButton)
			{
				String name = input.getText().toString().trim();
				startNewGame(name);
			}
		});
		dialog.show();
	}
	
	private void startNewGame(String name)
	{
		Intent newIntent = new Intent(this, ExploreActivity.class);
		newIntent.putExtra("name", name);
		startActivity(newIntent);
	}

	private void loadgameClicked()
	{
		Intent loadIntent = new Intent(this, ExploreActivity.class);
		loadIntent.putExtra("load game", true);
		startActivity(loadIntent);
	}

	private void scanClicked()
	{
		startActivity(new Intent(this, ScanActivity.class));
	}
	
	@Override
	protected void onResume()
	{
		super.onResume();
		
		Button b = (Button) findViewById(R.id.btn_loadgame);

		//re-check load game status
		File file = getFileStreamPath(ExploreActivity.SAVE_FILE);
		if (!file.exists())
			b.setEnabled(false);
		else
			b.setEnabled(true);
	}
}