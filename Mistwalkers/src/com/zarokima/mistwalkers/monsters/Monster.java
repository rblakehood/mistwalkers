package com.zarokima.mistwalkers.monsters;

import java.io.Serializable;
import java.util.ArrayList;
import org.anddev.andengine.entity.sprite.Sprite;
import org.anddev.andengine.opengl.texture.TextureOptions;
import org.anddev.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.anddev.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.anddev.andengine.opengl.texture.region.TextureRegion;
import com.zarokima.mistwalkers.skills.BasicAttack;
import com.zarokima.mistwalkers.skills.Skill;

//This is the base class for monsters
//Each species of monster should extend this class
public abstract class Monster implements Serializable
{
	protected String species;
	protected String name;
	protected int level;
	protected String imagePath;
	protected boolean AI;
	protected String description;
	
	//starting stats
	protected int baseHP;
	protected int baseMP;
	protected int basePhysAttack;
	protected int basePhysDefense;
	protected int baseMagAttack;
	protected int baseMagDefense;
	protected int baseAgility;
	
	//stat increases per level
	protected int growthHP;
	protected int growthMP;
	protected int growthPhysAttack;
	protected int growthPhysDefense;
	protected int growthMagAttack;
	protected int growthMagDefense;
	protected int growthAgility;
	
	//experience-related stuff -- can be changed in subclasses, these are just default
	protected int currentXP = 0;
	protected int baseXPToLevel = 10;
	protected float levelXPMultiplier = 1.5f;
	protected int rewardXP = 4; //reward for defeating this monster in battle, per level
	
	protected int currentHP;
	protected int currentMP;
	protected boolean alive;
	
	protected ArrayList<Skill> skills = new ArrayList<Skill>();

	protected Monster(int baseHP, int baseMP, int basePhysAttack, int basePhysDefense, int baseMagAttack,
			int baseMagDefense, int baseAgility, int growthHP, int growthMP, int growthPhysAttack,
			int growthPhysDefense, int growthMagAttack, int growthMagDefense, int growthAgility)
	{
		level = 1;
		alive = true;
		this.baseHP = baseHP;
		this.baseMP = baseMP;
		this.basePhysAttack = basePhysAttack;
		this.basePhysDefense = basePhysDefense;
		this.baseMagAttack = baseMagAttack;
		this.baseMagDefense = baseMagDefense;
		this.baseAgility = baseAgility;
		this.growthHP = growthHP;
		this.growthMP = growthMP;
		this.growthPhysAttack = growthPhysAttack;
		this.growthPhysDefense = growthPhysDefense;
		this.growthMagAttack = growthMagAttack;
		this.growthMagDefense = growthMagDefense;
		this.growthAgility = growthAgility;
		currentHP = getMaxHP();
		currentMP = getMaxMP();
		
		species = this.getClass().getSimpleName();
		name = species;
		imagePath = "gfx/monsters/" + species + ".png";
		description = "No description.";
		learnSkill(new BasicAttack());
	}
	
	//should only be used for advanced initialization in subclasses
	//will cause errors 
	protected Monster()
	{
		level = 1;
		alive = true;
		currentHP = getMaxHP();
		currentMP = getMaxMP();
		species = this.getClass().getSimpleName();
		name = species;
		imagePath = "gfx/monsters/" + species + ".png";
		description = "No description.";
		learnSkill(new BasicAttack());
	}
	
	protected void levelUp()
	{
		level += 1;
		currentHP = getMaxHP();
		currentMP = getMaxMP();
	}
	
	public void addXP(int xp)
	{
		currentXP += xp;
		
		while(currentXP >= getXPToLevel())
		{
			currentXP -= getXPToLevel();
			levelUp();
		}
	}
	
	//damages the monster
	//negative damage will heal
	//returns current HP
	public int inflictDamage(int damage)
	{
		currentHP -= damage;
		
		if(currentHP > getMaxHP())
		{
			currentHP = getMaxHP();
		}
		else if(currentHP < 0)
		{
			currentHP = 0;
			alive = false;
		}
		
		return currentHP;
	}
	
	public void learnSkill(Skill s)
	{
		skills.add(s);
	}
	
	//returns true if skill was successfully unlearned
	public boolean unlearnSkill(int i)
	{
		if(i <= 0 || i >= skills.size())
			return false;
		
		skills.remove(i);
		return true;
	}
	
	public String getName()
	{
		return name;
	}
	
	public void setName(String n)
	{
		name = n;
	}
	
	public String getSpecies()
	{
		return species;
	}
	
	public String getImagePath()
	{
		return imagePath;
	}
	
	public String getDescription()
	{
		return description;
	}
	
	//**********************************
	//these all return the current stats
	//**********************************
	public final boolean isAlive()
	{
		return alive;
	}
	
	public final void setAlive(boolean a)
	{
		alive = a;
	}
	
	public final int getMaxHP()
	{
		return baseHP + (level * growthHP);
	}
	
	public final int getCurrentHP()
	{
		return currentHP;
	}
	
	public final int getMaxMP()
	{
		return baseMP + (level * growthMP);
	}
	
	public final int getCurrentMP()
	{
		return currentMP;
	}
	
	public final int getPhysicalAttack()
	{
		return basePhysAttack + (level * growthPhysAttack);
	}
	
	public final int getPhysicalDefense()
	{
		return basePhysDefense + (level * growthPhysDefense);
	}
	
	public final int getMagicalAttack()
	{
		return baseMagAttack + (level * growthMagAttack);
	}
	
	public final int getMagicalDefense()
	{
		return baseMagDefense + (level * growthMagDefense);
	}
	
	public final int getAgility()
	{
		return baseAgility + (level * growthAgility);
	}
	
	public final ArrayList<Skill> getSkills()
	{
		return skills;
	}
	
	public final int getCurrentXP()
	{
		return currentXP;
	}
	
	//returns total experience needed to level up -- subtract currentXP to get experience remaining until next level up
	public final int getXPToLevel()
	{
		return (int) (baseXPToLevel + (baseXPToLevel * (level - 1) * levelXPMultiplier));
	}

	public final int getRewardXP()
	{
		return (int) (rewardXP * level * levelXPMultiplier);
	}

	//***************************************************
	//getters and setters for base stats and growth rates
    //***************************************************
	public final int getLevel()
	{
		return level;
	}

	public final void setLevel(int level)
	{
		this.level = level;
	}

	public final int getBaseHP()
	{
		return baseHP;
	}

	public final void setBaseHP(int baseHP)
	{
		this.baseHP = baseHP;
	}

	public final int getBaseMP()
	{
		return baseMP;
	}

	public final void setBaseMP(int baseMP)
	{
		this.baseMP = baseMP;
	}

	public final int getBasePhysAttack()
	{
		return basePhysAttack;
	}

	public final void setBasePhysAttack(int basePhysAttack)
	{
		this.basePhysAttack = basePhysAttack;
	}

	public final int getBasePhysDefense()
	{
		return basePhysDefense;
	}

	public final void setBasePhysDefense(int basePhysDefense)
	{
		this.basePhysDefense = basePhysDefense;
	}

	public final int getBaseMagAttack()
	{
		return baseMagAttack;
	}

	public final void setBaseMagAttack(int baseMagAttack)
	{
		this.baseMagAttack = baseMagAttack;
	}

	public final int getBaseMagDefense()
	{
		return baseMagDefense;
	}

	public final void setBaseMagDefense(int baseMagDefense)
	{
		this.baseMagDefense = baseMagDefense;
	}

	public final int getBaseAgility()
	{
		return baseAgility;
	}

	public final void setBaseAgility(int baseAgility)
	{
		this.baseAgility = baseAgility;
	}

	public final int getGrowthHP()
	{
		return growthHP;
	}

	public final void setGrowthHP(int growthHP)
	{
		this.growthHP = growthHP;
	}

	public final int getGrowthMP()
	{
		return growthMP;
	}

	public final void setGrowthMP(int growthMP)
	{
		this.growthMP = growthMP;
	}

	public final int getGrowthPhysAttack()
	{
		return growthPhysAttack;
	}

	public final void setGrowthPhysAttack(int growthPhysAttack)
	{
		this.growthPhysAttack = growthPhysAttack;
	}

	public final int getGrowthPhysDefense()
	{
		return growthPhysDefense;
	}

	public final void setGrowthPhysDefense(int growthPhysDefense)
	{
		this.growthPhysDefense = growthPhysDefense;
	}

	public final int getGrowthMagAttack()
	{
		return growthMagAttack;
	}

	public final void setGrowthMagAttack(int growthMagAttack)
	{
		this.growthMagAttack = growthMagAttack;
	}

	public final int getGrowthMagDefense()
	{
		return growthMagDefense;
	}

	public final void setGrowthMagDefense(int growthMagDefense)
	{
		this.growthMagDefense = growthMagDefense;
	}

	public final int getGrowthAgility()
	{
		return growthAgility;
	}

	public final void setGrowthAgility(int growthAgility)
	{
		this.growthAgility = growthAgility;
	}

	public final void setCurrentHP(int currentHP)
	{
		this.currentHP = currentHP;
	}

	public final void setCurrentMP(int currentMP)
	{
		this.currentMP = currentMP;
	}

	public final boolean isAI()
	{
		return AI;
	}

	public final void setAI(boolean aI)
	{
		AI = aI;
	}

	@Override
	public String toString()
	{
		String s = "Name: " + name + "\n";
		s += "Species: " + species + "\n";
		s += "Level: " + level + "\n";
		s += "HP: " + this.getMaxHP() + "\n";
		s += "MP: " + this.getMaxMP() + "\n";
		s += "P. Attack: " + this.getPhysicalAttack() + "\n";
		s += "P. Defense: " + this.getPhysicalDefense() + "\n";
		s += "M. Attack: " + this.getMagicalAttack() + "\n";
		s += "M. Defense: " + this.getMagicalDefense() + "\n";
		s += "Agility: " + this.getAgility() + "\n";
		
		return s;
	}
}
