package com.zarokima.mistwalkers.monsters;

import com.zarokima.mistwalkers.skills.TestPhysicalDamageSkill;

public class TestPlayerMonster3 extends Monster
{
	public TestPlayerMonster3()
	{
		super(30, 50, 20, 10, 0, 10, 15, 4, 6, 4, 2, 1, 2, 3);
		learnSkill(new TestPhysicalDamageSkill());
		description = "A test monster for the player.";
	}
}
