package com.zarokima.mistwalkers.monsters;

//slow weak monster with strong defense
public class MonsterB extends Monster
{
	public MonsterB()
	{
		super(100, 50, 5, 30, 5, 30, 2, 10, 5, 2, 5, 2, 5, 1);
		description = "This B a test monster.";
	}
}
