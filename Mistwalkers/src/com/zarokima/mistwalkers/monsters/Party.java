package com.zarokima.mistwalkers.monsters;

import java.io.Serializable;

//a simple container for a group of up to 3 monsters
public class Party implements Serializable
{
	private Monster monster1;
	private Monster monster2;
	private Monster monster3;

	public Party(Monster monster1, Monster monster2, Monster monster3)
	{
		this.monster1 = monster1;
		this.monster2 = monster2;
		this.monster3 = monster3;
	}

	public Monster getMonster1()
	{
		return monster1;
	}

	public void setMonster1(Monster monster1)
	{
		this.monster1 = monster1;
	}

	public Monster getMonster2()
	{
		return monster2;
	}

	public void setMonster2(Monster monster2)
	{
		this.monster2 = monster2;
	}

	public Monster getMonster3()
	{
		return monster3;
	}

	public void setMonster3(Monster monster3)
	{
		this.monster3 = monster3;
	}

	@Override
	public String toString()
	{
		return monster1.toString() + monster2.toString() + monster3.toString();
	}

}
