package com.zarokima.mistwalkers.monsters;

import com.zarokima.mistwalkers.skills.TestHealingSkill;
import com.zarokima.mistwalkers.skills.TestMagicalDamageSkill;

//balanced magic monster
public class MonsterC extends Monster
{
	public MonsterC()
	{
		super(50, 100, 5, 10, 15, 10, 8, 5, 10, 1, 2, 4, 2, 2);
		learnSkill(new TestMagicalDamageSkill());
		learnSkill(new TestHealingSkill());
		description = "C stands for cat.";
	}
}
