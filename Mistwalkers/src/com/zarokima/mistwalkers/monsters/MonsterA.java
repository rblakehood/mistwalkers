package com.zarokima.mistwalkers.monsters;

import com.zarokima.mistwalkers.skills.TestPhysicalDamageSkill;

//fast strong physical monster with weak defense
public class MonsterA extends Monster
{
	public MonsterA()
	{
		super(30, 50, 20, 10, 0, 10, 15, 4, 6, 4, 2, 1, 2, 3);
		learnSkill(new TestPhysicalDamageSkill());
		description = "A test monster that is just the letter A.";
	}
}
