package com.zarokima.mistwalkers.items;

import java.io.Serializable;
import java.util.ArrayList;
import com.zarokima.mistwalkers.monsters.Monster;

public class Inventory implements Serializable
{
	private final static int SIZE = 50;
	private final ArrayList<Item> items = new ArrayList<Item>(SIZE);
	
	public Inventory(){}
	
	public boolean addItem(Item item)
	{
		return items.add(item);
	}
	
	public void removeItem(int index)
	{
		items.remove(index);
	}
	
	public ArrayList<Item> getItems()
	{
		return items;
	}
	
	public boolean useItem(int index, Monster... targets)
	{	
		boolean success = true;
		Item item = items.get(index);
		
		if(item.isMultiTarget())
		{
			for(Monster target : targets)
			{
				if(!item.use(target))
					success = false;
			}
		}
		else
		{
			success = item.use(targets[0]);
		}
		
		items.remove(item);
		
		return success;
	}

	public int size()
	{
		return items.size();
	}

	public Item get(int index)
	{
		return items.get(index);
	}
}
