package com.zarokima.mistwalkers.items;

import java.io.Serializable;
import com.zarokima.mistwalkers.monsters.Monster;

public abstract class Item implements Serializable
{
	protected String name;
	protected String description;
	protected boolean consumeable; //flag for Inventory -- whether the item should be removed upon use
	protected boolean multiTarget; //flag for Inventory -- whether the item targets multiple monsters
	protected String imagePath;

	protected Item(boolean consumeable, boolean multiTarget)
	{
		this.consumeable = consumeable;
		this.multiTarget = multiTarget;
		
		name = this.getClass().getSimpleName();
		imagePath =  "gfx/items/" + name + ".png";
		description = "No description";
	}
	
	protected Item()
	{
		this(true, false);
	}

	public String getName()
	{
		return name;
	}

	public String getDescription()
	{
		return description;
	}

	public boolean isConsumeable()
	{
		return consumeable;
	}
	
	public boolean isMultiTarget()
	{
		return multiTarget;
	}

	public String getImagePath()
	{
		return imagePath;
	}
	
	//returns true if successfully used, false otherwise
	protected abstract boolean use(Monster target);
}
