package com.zarokima.mistwalkers.items;

import com.zarokima.mistwalkers.monsters.Monster;

public class MediumHealthPotion extends Item
{
	public MediumHealthPotion()
	{
		super();
		name = "Medium Health Potion";
		description = "Heals one monster for 200 points.";
	}
	
	@Override
	public boolean use(Monster target)
	{
		target.inflictDamage(-200);
		return true;
	}

}
