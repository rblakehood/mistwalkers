package com.zarokima.mistwalkers.items;

import com.zarokima.mistwalkers.monsters.Monster;

public class SmallHealthPotion extends Item
{
	public SmallHealthPotion()
	{
		super();
		name = "Small Health Potion";
		description = "Heals one monster for 50 points.";
	}

	@Override
	public boolean use(Monster target)
	{
		target.inflictDamage(-50);
		return true;
	}

}
