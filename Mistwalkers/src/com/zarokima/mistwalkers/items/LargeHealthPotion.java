package com.zarokima.mistwalkers.items;

import com.zarokima.mistwalkers.monsters.Monster;

public class LargeHealthPotion extends Item
{
	public LargeHealthPotion()
	{
		super();
		name = "Large Health Potion";
		description = "Heals one monster for 500 points.";
	}

	@Override
	public boolean use(Monster target)
	{
		target.inflictDamage(-500);
		return true;
	}

}
