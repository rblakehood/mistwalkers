package com.zarokima.mistwalkers.scan;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import com.zarokima.mistwalkers.MainMenuActivity;
import android.app.Activity;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.content.Intent;

public class ScanActivity extends Activity
{
	private final static String TAG = MainMenuActivity.class.getSimpleName();
	public final static String BARCODE_FILE_PATH = "barcode.txt";
	
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		
		Intent intent = new Intent("com.google.zxing.client.android.SCAN");
		intent.putExtra("SCAN_MODE", "PRODUCT_MODE");
		startActivityForResult(intent, 0);
	}
	
	public void onActivityResult(int requestCode, int resultCode, Intent intent) 
	{
		   if (requestCode == 0) {
		      if (resultCode == RESULT_OK) {
		         String contents = intent.getStringExtra("SCAN_RESULT");
		         
		         try
		         {
		        	 String externalDir = Environment.getExternalStorageDirectory().toString();
		        	 String fileName = BARCODE_FILE_PATH;
		        	 
		        	 File file = new File(externalDir,fileName);
		        	 if(!file.exists())
		        	 {
		        		 file.createNewFile();
		        	 }

		        	 FileWriter writer = new FileWriter(file, true);
		        	 
		        	 contents = normalizeScans(contents);
		        	 writer.append(contents);
		        	 writer.append("\n");
		        	 writer.flush();
		        	 writer.close();
		        	 finish();
		         }
		         catch (IOException error)
		         {
		        	 Log.e(TAG, "Error saving game: " + error.toString());
		        	 finish();
		         }
		         		         
		      } else if (resultCode == RESULT_CANCELED) {
		         //need to do something here...
		    	  finish();
		      }
		   }
		}

	private String normalizeScans(String contents)
	{
		long l = Long.parseLong(contents);
		String s = String.valueOf(l);
		
		return s;
	}
	
}
