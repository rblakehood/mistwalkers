package com.zarokima.mistwalkers.explore;

import java.io.IOException;
import java.io.Serializable;
import java.util.Random;
import java.util.Stack;
import org.anddev.andengine.util.path.Direction;

public class MazeGenerator implements Serializable
{
	private int width, height; // the dimensions of the maze
	private MazeCell[][] maze = null;
	private MazeCell startCell = null, endCell = null;
	private Random rand = new Random();
	private Stack<MazeCell> stack;
	private long seed = rand.nextLong();

	public MazeGenerator(int width, int height)
	{
		this.width = width;
		this.height = height;
	}

	public void setSeed(long seed)
	{
		this.seed = seed;
	}

	public void setSize(int width, int height)
	{
		this.width = width;
		this.height = height;
	}

	public Long getSeed()
	{
		return seed;
	}

	public int getStartIndex()
	{
		return startCell.getMapIndex();
	}

	public MazeCell getStartCell()
	{
		return startCell;
	}

	public MazeCell getCell(int width, int height)
	{
		return maze[width][height];
	}

	public String outputMazeText()
	{
		String output = new String();
		for (int i = 0; i < height; i++)
		{
			// draw the north edge
			for (int k = 0; k < width; k++)
			{
				if (maze[k][i] == startCell)
				{
					output += "S";
				}
				else if (maze[k][i] == endCell)
				{
					output += "E";
				}
				else
				{
					output += maze[k][i].getSymbol();
				}
			}
			output += "\n";

		}

		return output;
	}

	public void generateMaze()
	{
		startCell = null;
		endCell = null;
		rand = new Random(seed);
		maze = new MazeCell[width][height];
		for (int i = 0; i < width; i++)
		{
			for (int k = 0; k < height; k++)
			{
				maze[i][k] = new MazeCell(i, k);
			}
		}

		MazeCell.setBounds(width, height);

		stack = new Stack<MazeCell>();
		startCell = maze[rand.nextInt(width)][rand.nextInt(height)];
		stack.push(startCell);

		// depth-first perfect maze generation
		while (!stack.isEmpty())
		{
			MazeCell currentCell = stack.peek();

			currentCell.setInMaze(true);

			Direction[] possibleDirections = currentCell.getUncheckedDirections();

			if (possibleDirections.length == 0)
			{
				stack.pop();
				continue;
			}

			int dint = rand.nextInt(possibleDirections.length);
			Direction direction = possibleDirections[dint];
			Direction opposite = null;

			MazeCell nextCell = null;
			SerialPoint position = currentCell.getPosition();

			switch (direction)
			{
				case UP:
					nextCell = maze[position.x][position.y - 1];
					opposite = Direction.DOWN;
					break;
				case DOWN:
					nextCell = maze[position.x][position.y + 1];
					opposite = Direction.UP;
					break;
				case LEFT:
					nextCell = maze[position.x - 1][position.y];
					opposite = Direction.RIGHT;
					break;
				case RIGHT:
					nextCell = maze[position.x + 1][position.y];
					opposite = Direction.LEFT;
					break;
			}

			if (!nextCell.isInMaze())
			{
				currentCell.setNeighbor(nextCell, direction);
				nextCell.setNeighbor(currentCell, opposite);

				stack.push(nextCell);
			}
			else
			{
				currentCell.setDirectionChecked(direction, true);
			}
		}

		// additional connections to make navigation less tedious
		try
		{
			int connections =  (int) Math.ceil(Math.sqrt((width * width) + (height * height)));
			while (connections > 0)
			{
				int x = rand.nextInt(width);
				int y = rand.nextInt(height);
				MazeCell currentCell = maze[x][y];
				Direction direction = Direction.values()[rand.nextInt(4)];

				if (currentCell.hasNeighbor(direction) || (direction == Direction.UP && y == 0)
						|| (direction == Direction.DOWN && y == (height-1)) || (direction == Direction.LEFT && x == 0)
						|| (direction == Direction.RIGHT && x == (width-1)))
				{
					continue;
				}

				Direction opposite = null;

				MazeCell nextCell = null;
				SerialPoint position = currentCell.getPosition();

				switch (direction)
				{
					case UP:
						nextCell = maze[position.x][position.y - 1];
						opposite = Direction.DOWN;
						break;
					case DOWN:
						nextCell = maze[position.x][position.y + 1];
						opposite = Direction.UP;
						break;
					case LEFT:
						nextCell = maze[position.x - 1][position.y];
						opposite = Direction.RIGHT;
						break;
					case RIGHT:
						nextCell = maze[position.x + 1][position.y];
						opposite = Direction.LEFT;
						break;
				}

				currentCell.setNeighbor(nextCell, direction);
				nextCell.setNeighbor(currentCell, opposite);

				connections--;
			}

			for (int i = 0; i < width; i++)
			{
				for (int k = 0; k < height; k++)
				{
					maze[i][k].determineSymbol();
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	private void writeObject(java.io.ObjectOutputStream out) throws IOException
	{
		out.writeInt(width);
		out.writeInt(height);
		out.writeLong(seed);
	}

	private void readObject(java.io.ObjectInputStream in) throws IOException, ClassNotFoundException
	{
		width = in.readInt();
		height = in.readInt();
		seed = in.readLong();
		generateMaze();
	}

}