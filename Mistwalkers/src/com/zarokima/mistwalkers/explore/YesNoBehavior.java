package com.zarokima.mistwalkers.explore;
import java.util.ArrayList;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.util.Log;


public class YesNoBehavior extends SpeakBehavior
{
	DialogInterface.OnClickListener yesListener;
	DialogInterface.OnClickListener noListener;
	
	public YesNoBehavior(String title, String message, DialogInterface.OnClickListener yes, DialogInterface.OnClickListener no)
	{
		super(title, message);
		yesListener = yes;
		noListener = no;
	}

	@Override
	protected void speak(final Activity activity, final Context context, final ArrayList<SpeakBehavior> behaviors)
	{
		AlertDialog.Builder builder = new AlertDialog.Builder(activity);
		builder.setTitle(title);
		builder.setMessage(message);
		
		builder.setPositiveButton("Yes", yesListener);
		builder.setNegativeButton("No", noListener);
		builder.setCancelable(false);
		
		AlertDialog alert = builder.create();
		alert.setOnDismissListener(new OnDismissListener()
		{

			@Override
			public void onDismiss(DialogInterface dialog)
			{
				int index = behaviors.indexOf(YesNoBehavior.this);
				Log.d("ALERT", "index " + index + " of " + behaviors.size());
				if (index < (behaviors.size() - 1))
				{
					behaviors.get(index+1).doBehavior(activity, context, behaviors);
				}
			}
		});
		alert.show();
	}
}
