package com.zarokima.mistwalkers.explore;

import org.anddev.andengine.opengl.texture.TextureOptions;
import org.anddev.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import android.content.Context;
import android.graphics.PointF;
import com.zarokima.mistwalkers.items.Inventory;
import com.zarokima.mistwalkers.items.LargeHealthPotion;
import com.zarokima.mistwalkers.items.MediumHealthPotion;
import com.zarokima.mistwalkers.items.SmallHealthPotion;
import com.zarokima.mistwalkers.monsters.MonsterA;
import com.zarokima.mistwalkers.monsters.MonsterB;
import com.zarokima.mistwalkers.monsters.MonsterC;
import com.zarokima.mistwalkers.monsters.Party;

//represents the player's character
public class Player extends Character
{
	private final static String TAG = Player.class.getSimpleName();
	private final static String ASSET_PATH = "gfx/MWarrior.png";
	private final static int TEXTURE_WIDTH = 64;
	private final static int TEXTURE_HEIGHT = 128;
	private Party party;
	private Inventory inventory;

	public Player(Context c, String name)
	{
		super(c, new BitmapTextureAtlas(TEXTURE_WIDTH, TEXTURE_HEIGHT, TextureOptions.DEFAULT), ASSET_PATH, name);
		playerSetup();
	}

	public Player(Context c, PointF p, String name)
	{
		super(c, p, new BitmapTextureAtlas(TEXTURE_WIDTH, TEXTURE_HEIGHT, TextureOptions.DEFAULT), ASSET_PATH, name);
		playerSetup();
	}

	private void playerSetup()
	{
		ANIMATE_DURATION = new long[] { 150, 150, 150 };
		party = new Party(new MonsterA(), new MonsterB(), new MonsterC());
		party.getMonster1().addXP(200);
		party.getMonster2().addXP(789);
		party.getMonster3().addXP(17);
		
		inventory = new Inventory();
		for(int i = 0; i < 10; i++)
		{
			inventory.addItem(new SmallHealthPotion());
			inventory.addItem(new MediumHealthPotion());
			inventory.addItem(new LargeHealthPotion());
		}
	}
	
	public Party getParty()
	{
		return party;
	}
	
	public Inventory getInventory()
	{
		return inventory;
	}
}
