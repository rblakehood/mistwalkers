package com.zarokima.mistwalkers.explore;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;
import org.anddev.andengine.audio.music.Music;
import org.anddev.andengine.audio.music.MusicFactory;
import org.anddev.andengine.engine.Engine;
import org.anddev.andengine.engine.camera.BoundCamera;
import org.anddev.andengine.engine.handler.IUpdateHandler;
import org.anddev.andengine.engine.handler.timer.ITimerCallback;
import org.anddev.andengine.engine.handler.timer.TimerHandler;
import org.anddev.andengine.engine.options.EngineOptions;
import org.anddev.andengine.engine.options.EngineOptions.ScreenOrientation;
import org.anddev.andengine.engine.options.resolutionpolicy.RatioResolutionPolicy;
import org.anddev.andengine.entity.layer.tiled.tmx.TMXLayer;
import org.anddev.andengine.entity.layer.tiled.tmx.TMXLoader;
import org.anddev.andengine.entity.layer.tiled.tmx.TMXTile;
import org.anddev.andengine.entity.layer.tiled.tmx.TMXTiledMap;
import org.anddev.andengine.entity.modifier.IEntityModifier;
import org.anddev.andengine.entity.modifier.PathModifier;
import org.anddev.andengine.entity.modifier.PathModifier.Path;
import org.anddev.andengine.entity.scene.Scene;
import org.anddev.andengine.entity.scene.Scene.IOnSceneTouchListener;
import org.anddev.andengine.entity.scene.background.ColorBackground;
import org.anddev.andengine.entity.sprite.Sprite;
import org.anddev.andengine.entity.util.FPSLogger;
import org.anddev.andengine.input.touch.TouchEvent;
import org.anddev.andengine.opengl.texture.TextureOptions;
import org.anddev.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.anddev.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.anddev.andengine.opengl.texture.region.TextureRegion;
import org.anddev.andengine.ui.activity.BaseGameActivity;
import org.anddev.andengine.util.Debug;
import org.anddev.andengine.util.path.Direction;
import org.anddev.andengine.util.path.astar.AStarPathFinder;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import com.zarokima.mistwalkers.R;

public class ExploreActivity extends BaseGameActivity implements IOnSceneTouchListener
{
	// ===========================================================
	// Constants
	// ===========================================================

	public static final String SAVE_FILE = "savedata";
	private static final String MAZE_FILE = "mazedata";
	private static final String TAG = ExploreActivity.class.getSimpleName();
	protected static final int TILE_WIDTH = 16;
	protected static final int TILE_HEIGHT = 16;
	protected static final float TOUCH_SPEED = 0.25f;
	protected static final int START_MAP_INDEX = 1;
	protected static final String townMusicPath = "OrchestraLoop.ogg";
	protected static final String mazeMusicPath = "HouseInaForestLoop.ogg";

	// ===========================================================
	// Camera
	// ===========================================================
	private static int CAMERA_WIDTH;
	private static int CAMERA_HEIGHT;

	// ===========================================================
	// Fields
	// ===========================================================

	protected static Music music;
	protected static String currentMusicPath;
	protected static BoundCamera camera;
	protected static Scene scene;
	protected static Player player;
	protected static Context context;
	protected static Sprite fadeScreen;
	private BitmapTextureAtlas fadeTexture;
	private TextureRegion fadeTextureRegion;
	public static Typeface font;
	protected static Menu menu;

	// TMX Fields
	protected static TMXLoader tmxLoader;
	protected static TMXTiledMap map;
	protected static TMXLayer TMXMapLayer;
	protected static TMXLayer mAnimateLayer;
	protected static TMXTile playerLocationTile;
	protected static MazeGenerator currentMaze;
	protected static MazeCell currentCell;
	protected static Direction mazeDirection = Direction.DOWN; //just some initial value to avoid a null pointer error in an if statement

	// Lists
	protected static List<TMXTile> CollideTiles;
	protected static List<TMXTile> ExitTiles;
	protected static ArrayList<Integer> MapNumber;
	protected static List<TMXTile> animatedTiles;
	protected static List<TMXTile> playerSpawnTiles;
	protected static ArrayList<Integer> LinkNumber;
	protected static List<TMXTile> propertyTiles;
	protected static ArrayList<Integer> propertyIndex;
	protected static ArrayList<NonPlayerCharacter> NPCs;
	protected static List<TMXLayer> topLayers;
	protected static ArrayList<MazeCell> neighborCells;
	protected static ArrayList<Direction> neighborCellDirections;
	protected static ArrayList<ExplorePortal> portals = new ArrayList<ExplorePortal>();

	// Sprite Control/UI/Pathing Fields
	protected static int playerSpawnDirection;
	protected static org.anddev.andengine.util.path.Path A_path;
	protected static AStarPathFinder<TMXLayer> finder;
	protected static float[] playerFootCordinates;
	protected static Path currentPath;
	protected static boolean isWalking;
	protected static int waypointIndex;
	protected static PathModifier moveModifier;
	protected static IEntityModifier pathTemp;

	// Indexes
	protected static int currentMapIndex;
	protected static int linkIndex;

	// Initial values; These should be initialized from a separate file that was
	// saved before finish();
	protected static float InitialplayerSpawnX;
	protected static float InitialplayerSpawnY;

	// Used to get object tiles in TMX
	protected static int ObjectX;
	protected static int ObjectY;
	protected static int ObjectWidth;
	protected static int ObjectHeight;

	// Boolean Flags
	protected static boolean EnableBounds = true;
	protected static boolean AssetsAttached;
	protected static boolean MapIsLoading;
	protected static boolean TouchOnce;
	protected static boolean UpdateTiles;
	protected static boolean loadNPCs;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		// get extras to determine if we're loading or starting a new game
		Bundle extras = getIntent().getExtras();
		if (extras == null)
		{
			Log.d(TAG, "Intent has no extras");
			this.finish();
		}
		else if (extras.getBoolean("load game"))
		{
			loadGame();
		}
		else
		{
			player = new Player(this, extras.getString("name"));
			currentMapIndex = START_MAP_INDEX;
			InitialplayerSpawnX = TILE_WIDTH * 2;
			InitialplayerSpawnY = TILE_HEIGHT * 2;
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu m)
	{
		menu = m;
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.explore_menu, m);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		// Handle item selection
		switch (item.getItemId())
		{
			case R.id.save:
				saveGame();
				break;
			case R.id.returntotown:
				returnToTown();
				break;
			case R.id.status:
				Intent newIntent = new Intent(this, StatusScreenActivity.class);
				newIntent.putExtra("Party", player.getParty());
				startActivity(newIntent);
				break;
			case R.id.inventory:
				newIntent = new Intent(this, InventoryActivity.class);
				newIntent.putExtra("Inventory", player.getInventory());
				newIntent.putExtra("Party", player.getParty());
				startActivity(newIntent);
				break;
			default:
				return super.onOptionsItemSelected(item);
		}
		
		return true;
	}

	@Override
	public Engine onLoadEngine()
	{
		// determine camera size
		DisplayMetrics metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);
		CAMERA_WIDTH = metrics.widthPixels / 4;
		CAMERA_HEIGHT = metrics.heightPixels / 4;

		context = getApplicationContext();

		camera = new BoundCamera(0, 0, CAMERA_WIDTH, CAMERA_HEIGHT);
		EngineOptions eo = new EngineOptions(true, ScreenOrientation.LANDSCAPE, new RatioResolutionPolicy(CAMERA_WIDTH,
				CAMERA_HEIGHT), camera);
		eo.getTouchOptions().setRunOnUpdateThread(true);
		eo.setNeedsMusic(true);
		return new Engine(eo);
	}

	@Override
	public void onLoadResources()
	{
		this.mEngine.getTextureManager().loadTexture(player.getTextureAtlas());

		MusicFactory.setAssetBasePath("mfx/");
		try
		{
			music = MusicFactory.createMusicFromAsset(this.mEngine.getMusicManager(), this, townMusicPath);
			music.setLooping(true);
			currentMusicPath = townMusicPath;
		}
		catch (final IOException e)
		{
			Debug.e(e);
		}

		fadeTexture = new BitmapTextureAtlas(1024, 1024, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
		fadeTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(fadeTexture,
				getApplicationContext(), "gfx/FadeBackground.png", 0, 0);
		mEngine.getTextureManager().loadTexture(fadeTexture);
		
		font = Typeface.createFromAsset(getAssets(), "RailModelFont.ttf");
	}

	@Override
	public Scene onLoadScene()
	{
		// Logs the FPS in LogCat
		this.mEngine.registerUpdateHandler(new FPSLogger());

		Character.setActivity(this);

		// Create the scene
		scene = new Scene();

		scene.setBackground(new ColorBackground(0f, 0f, 0f));

		// Initializes the TMXLoader
		tmxLoader = new TMXLoader(this, this.mEngine.getTextureManager(), TextureOptions.NEAREST, null);

		fadeScreen = new Sprite(0, 0, fadeTextureRegion);

		// Initial load. In real game the current map index would be saved right
		// before exiting and that index would be passed into this method
		MapHandler.loadAndAttachAssets(currentMapIndex);

		scene.registerUpdateHandler(new IUpdateHandler()
		{

			@Override
			public void reset()
			{
			}

			@Override
			public void onUpdate(final float pSecondsElapsed)
			{
				// If the TMX map is loading then the scene updates will freeze
				if (!MapIsLoading)
				{

					// Gets the index of the current map
					currentMapIndex = Integer.parseInt(map.getTMXTiledMapProperties().get(0).getValue());
					
					// This updates the TMX map when the sprite exits or enters
					// the scene
					MapHandler.updateTMXMap();

					// check for interaction
					MapHandler.checkInteractions();
					
					if(currentMapIndex < 100 && currentMusicPath != townMusicPath)
					{
						try
						{
							music.stop();
							music = MusicFactory.createMusicFromAsset(mEngine.getMusicManager(), ExploreActivity.this, townMusicPath);
							music.setLooping(true);
							currentMusicPath = townMusicPath;
							music.play();
						}
						catch (IllegalStateException e)
						{
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						catch (IOException e)
						{
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					else if (currentMapIndex >= 100 && currentMusicPath != mazeMusicPath)
					{
						try
						{
							music.stop();
							music = MusicFactory.createMusicFromAsset(mEngine.getMusicManager(), ExploreActivity.this, mazeMusicPath);
							music.setLooping(true);
							currentMusicPath = mazeMusicPath;
							music.play();
						}
						catch (IllegalStateException e)
						{
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						catch (IOException e)
						{
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
				if (loadNPCs)
				{
					// load assets for any NPCs on the map
					for (NonPlayerCharacter npc : NPCs)
					{
						mEngine.getTextureManager().loadTexture(npc.getTextureAtlas());
					}
					loadNPCs = false;
				}
			}
		});

		scene.registerUpdateHandler(player);

		scene.setOnSceneTouchListener(this);

		music.play();

		return scene;
	}

	@Override
	public void onLoadComplete()
	{
		// TODO Auto-generated method stub
	}

	@Override
	public boolean onSceneTouchEvent(Scene sce, TouchEvent event)
	{
		// ignore if map is loading.
		if (MapIsLoading)
			return true;

		// on newer android devices where the system tray is part of the screen, we still receive touch events if the
		// user drags onto the system tray, which gives coordinates that are outside of the map. This makes sure the
		// event is good.
		if (event.getX() < 0 || event.getX() > map.getTileColumns() * map.getTileWidth() || event.getY() < 0
				|| event.getY() > map.getTileRows() * map.getTileHeight())
		{
			return true;
		}
		// only move on taps
		if (event.isActionDown())
		{
			player.walkTo(event.getX(), event.getY(), sce);
		}

		return true;
	}

	// writes information to save file so current state can be reloaded
	private void saveGame()
	{	
		try
		{
			OutputStreamWriter osw = new OutputStreamWriter(openFileOutput(SAVE_FILE, Context.MODE_PRIVATE));

			// One item per line, in the specific order of:
			// player name, index of the current map, player x position, player y position
			String output = player.getName();
			output += "\n" + currentMapIndex;
			output += "\n" + player.getPosition().x + "\n" + player.getPosition().y;

			osw.write(output);
			osw.close();
			
			//If in a maze, save the maze
			if(currentMapIndex > 99)
			{
				ObjectOutputStream oos = new ObjectOutputStream(openFileOutput(MAZE_FILE, Context.MODE_PRIVATE));
				oos.writeObject(currentMaze);
				oos.writeObject(currentCell.getPosition());
				oos.close();
			}
			
			for(ExplorePortal portal : portals)
			{
				try
				{
				osw = new OutputStreamWriter(openFileOutput(portal.getName(), Context.MODE_PRIVATE));
				osw.write(portal.getSeed() + "");
				osw.close();
				}
				catch (NullPointerException e)
				{
					//portal wasn't actually initialized, so nothing to save
				}
			}
		}
		catch (FileNotFoundException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// reads from the save file to determine how to set the scene
	private void loadGame()
	{
		try
		{
			BufferedReader buf = new BufferedReader(new InputStreamReader(openFileInput(SAVE_FILE)));

			// see comment in saveGame() for order of data
			String name = buf.readLine();
			currentMapIndex = Integer.parseInt(buf.readLine());
			InitialplayerSpawnX = Float.parseFloat(buf.readLine());
			InitialplayerSpawnY = Float.parseFloat(buf.readLine());
			buf.close();

			if(currentMapIndex > 99)
			{
				ObjectInputStream ois = new ObjectInputStream(openFileInput(MAZE_FILE));
				currentMaze = (MazeGenerator) ois.readObject();
				SerialPoint position = (SerialPoint) ois.readObject();
				ois.close();
				
				currentCell = currentMaze.getCell(position.x, position.y);
			}
			
			player = new Player(this, name);
		}
		catch (FileNotFoundException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (ClassNotFoundException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void returnToTown()
	{
		if(currentMapIndex > 99)
		{
			MapHandler.setCurrentCell(null);
			MapHandler.setCurrentMaze(null);
			//neighborCells = new ArrayList<MazeCell>();
			//neighborCellDirections = new ArrayList<Direction>();
			MapHandler.addExitTile(player.getLocationTile(), 13);
		}
		else
		{
			SpeakBehavior sb = new SpeakBehavior("", "You're already in town.");
			sb.doBehavior(this, context, null);
		}
	}
	
	@Override
	public void onResumeGame()
	{
		super.onResumeGame();
		if(music != null && !music.isPlaying())
		{
			music.resume();
		}
	}

	@Override
	protected void onPause()
	{
		super.onPause();
		if (music != null)
		{
			music.pause();
		}
	}
}