package com.zarokima.mistwalkers.explore;

import android.graphics.PointF;
import android.graphics.RectF;

//used by anything the player can interact with
public interface Interactable
{
	//performs the interaction
	public void interact();
	
	//to determine if the player is in contact and should trigger an interaction
	public PointF getPosition();
}
