package com.zarokima.mistwalkers.explore;

import java.util.ArrayList;
import org.anddev.andengine.entity.scene.Scene;
import org.anddev.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import android.content.Context;
import android.graphics.PointF;

public class NonPlayerCharacter extends Character implements Interactable
{
	private final static String TAG = Character.class.getSimpleName();
	protected PointF[] pathPoints = null;
	protected int currentPoint = 0;
	protected Scene scene;
	protected boolean interactOnce = false;
	protected ArrayList<SpeakBehavior> speakBehaviors = new ArrayList<SpeakBehavior>();

	public NonPlayerCharacter(Context c, BitmapTextureAtlas bta, String path, String n)
	{
		super(c, bta, path, n);
	}

	public NonPlayerCharacter(Context c, BitmapTextureAtlas bta, String path)
	{
		super(c, bta, path);
	}

	public NonPlayerCharacter(Context c, PointF p, BitmapTextureAtlas bta, String path, String n)
	{
		super(c, p, bta, path, n);
	}

	public NonPlayerCharacter(Context c, PointF p, BitmapTextureAtlas bta, String path)
	{
		super(c, p, bta, path);
	}

	public void setPathPoints(PointF... points)
	{
		pathPoints = points;
		speed = 0.5f;
	}

	public void setScene(Scene scene)
	{
		this.scene = scene;
	}

	public void addMessage(String message)
	{
		speakBehaviors.add(new SpeakBehavior(name, message));
	}
	
	public void addSpeakBehavior(SpeakBehavior sb)
	{
		speakBehaviors.add(sb);
	}
	
	public void addSpeakBehavior(String title, String message)
	{
		speakBehaviors.add(new SpeakBehavior(title, message));
	}

	public boolean hasInteracted()
	{
		return interactOnce;
	}

	// the player speaks with the NPC
	public void interact()
	{
		if (interactOnce || speakBehaviors.size() == 0)
		{
			return;
		}

		interactOnce = true;
		
		speakBehaviors.get(0).doBehavior(activity, context, speakBehaviors);
	}

	@Override
	public void onUpdate(float dt)
	{
		super.onUpdate(dt);

		if(MapHandler.isLoadingMap())
		{
			return;
		}
		
		if (interactOnce && ExploreActivity.player.getLocationTile() != this.getLocationTile())
		{
			interactOnce = false;
		}

		if (pathPoints == null)
			return;

		if(pathPoints[currentPoint].equals(sprite.getX(), sprite.getY()))
		{
			if(currentPoint == pathPoints.length - 1)
			{
				currentPoint = 0;
			}
			else
			{
				currentPoint++;
			}
			
			walkTo(pathPoints[currentPoint].x, pathPoints[currentPoint].y, scene);
		}
	}
}
