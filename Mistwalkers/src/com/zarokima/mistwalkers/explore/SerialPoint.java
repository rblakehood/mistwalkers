package com.zarokima.mistwalkers.explore;

import java.io.Serializable;

public class SerialPoint implements Serializable
{
	public int x, y;
	
	public SerialPoint(int x, int y)
	{
		this.x = x;
		this.y = y;
	}
}
