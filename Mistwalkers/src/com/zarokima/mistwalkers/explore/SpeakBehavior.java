package com.zarokima.mistwalkers.explore;

import java.util.ArrayList;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.graphics.Typeface;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.widget.TextView;

/*
 * Contains the behavior for NPCs to speak to the player.
 * This is just a simple message display in an alert dialog.
 * Can be subclassed for different behavior.
 */
public class SpeakBehavior
{
	protected Typeface font = null;
	protected String title;
	protected String message;
	protected int fontSize = 20;

	public SpeakBehavior(String title, String message)
	{
		this.title = title;
		this.message = message;
	}

	public Typeface getFont()
	{
		return font;
	}

	public String getTitle()
	{
		return title;
	}

	public String getMessage()
	{
		return message;
	}

	public int getFontSize()
	{
		return fontSize;
	}

	public void setFont(Typeface font)
	{
		this.font = font;
	}

	public void setTitle(String title)
	{
		this.title = title;
	}

	public void setMessage(String message)
	{
		this.message = message;
	}

	public void setFontSize(int fontSize)
	{
		this.fontSize = fontSize;
	}

	// do not touch -- just sets up to call speak()
	public final void doBehavior(final Activity activity, final Context context,
			final ArrayList<SpeakBehavior> behaviors)
	{
		// magic happens here
		new Thread()
		{
			public void run()
			{
				activity.runOnUiThread(new Runnable()
				{
					public void run()
					{
						speak(activity, context, behaviors);
					}
				});
			}
		}.start();
	}

	// override this in subclasses
	// behaviors is assumed to contain this behavior
	// if it doesn't, the first behavior contained will be repeated infinitely
	protected void speak(final Activity activity, final Context context, final ArrayList<SpeakBehavior> behaviors)
	{
		AlertDialog alert = new AlertDialog.Builder(activity).create();
		alert.setOnDismissListener(new OnDismissListener()
		{

			@Override
			public void onDismiss(DialogInterface dialog)
			{
				if (behaviors != null)
				{
					int index = behaviors.indexOf(SpeakBehavior.this);
					Log.d("ALERT", "index " + index + " of " + behaviors.size());
					if (index < (behaviors.size() - 1))
					{
						behaviors.get(index + 1).doBehavior(activity, context, behaviors);
					}
				}
			}

		});
		alert.setTitle(title);
		TextView tv = new TextView(context);
		tv.setText(message);
		tv.setTextSize(fontSize);
		if (font != null)
		{
			tv.setTypeface(font);
		}
		tv.setMovementMethod(new ScrollingMovementMethod());
		alert.setView(tv);
		alert.show();
	}
}
