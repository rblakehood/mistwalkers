package com.zarokima.mistwalkers.explore;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import org.anddev.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.anddev.andengine.util.path.Direction;
import com.zarokima.mistwalkers.scan.ScanActivity;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.graphics.PointF;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.widget.TextView;

//test class used for outputting text mazes
//generates and shows a maze when you talk to him
public class NPCMazeMaker extends NonPlayerCharacter
{
	private MazeGenerator maze;
	private String firstMessage = "The white lines are the path of the maze. The seed is shown at the top.";

	public NPCMazeMaker(Context c, BitmapTextureAtlas bta, String path, String n)
	{
		super(c, bta, path, n);
		mazeSetup();
	}

	public NPCMazeMaker(Context c, BitmapTextureAtlas bta, String path)
	{
		super(c, bta, path);
		mazeSetup();
	}

	public NPCMazeMaker(Context c, PointF p, BitmapTextureAtlas bta, String path, String n)
	{
		super(c, p, bta, path, n);
		mazeSetup();
	}

	public NPCMazeMaker(Context c, PointF p, BitmapTextureAtlas bta, String path)
	{
		super(c, p, bta, path);
		mazeSetup();
	}
	
	private void mazeSetup()
	{
		speakBehaviors.add(new SelectSeedBehavior("Select a UPC to use", "Select a UPC to use."));
		speakBehaviors.add(new SpeakBehavior(name, firstMessage));
		maze = new MazeGenerator(10, 10);
	}

	@Override
	public void interact()
	{
			super.interact();
	}
	
	private void generateMaze(long seed)
	{
		maze.setSeed(seed);
		maze.generateMaze();
		
		//remove current maze, if one exists
		if (speakBehaviors.size() == 3)
		{
			speakBehaviors.remove(2);
		}
		SpeakBehavior speakMaze = new SpeakBehavior(name, maze.outputMazeText());
		speakMaze.setFont(ExploreActivity.font);
		speakMaze.setFontSize(40);
		speakBehaviors.add(speakMaze);
	}
	
	private class SelectSeedBehavior extends SpeakBehavior
	{

		public SelectSeedBehavior(String title, String message)
		{
			super(title, message);
		}

		@Override
		protected void speak(final Activity activity, final Context context, final ArrayList<SpeakBehavior> behaviors)
		{
			try
			{
				ArrayList<String> barcodes = new ArrayList<String>();
				
				BufferedReader buf = new BufferedReader(new InputStreamReader(activity.openFileInput(ScanActivity.BARCODE_FILE_PATH)));
				String input = buf.readLine();
				while(input != null)
				{
					barcodes.add(input);
					input = buf.readLine();
				}
				buf.close();
				
				AlertDialog.Builder builder = new AlertDialog.Builder(activity);
				builder.setTitle(title);
				
				final String[] barcodeArray = barcodes.toArray(new String[0]);
				
				builder.setItems(barcodeArray, new DialogInterface.OnClickListener()
				{
					
					@Override
					public void onClick(DialogInterface dialog, int which)
					{
						generateMaze(Long.parseLong(barcodeArray[which]));
						
						dialog.dismiss();
					}
				});
				AlertDialog alert = builder.create();
				alert.setOnDismissListener(new OnDismissListener()
				{

					@Override
					public void onDismiss(DialogInterface dialog)
					{
						int index = behaviors.indexOf(SelectSeedBehavior.this);
						Log.d("ALERT", "index " + index + " of " + behaviors.size());
						if (index < (behaviors.size() - 1))
						{
							behaviors.get(index+1).doBehavior(activity, context, behaviors);
						}
					}
				});
				alert.show();
			}
			catch (FileNotFoundException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			catch (IOException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
