package com.zarokima.mistwalkers.explore;

import java.util.ArrayList;
import java.util.Random;
import org.anddev.andengine.entity.scene.Scene;
import org.anddev.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.anddev.andengine.util.path.Direction;
import com.zarokima.mistwalkers.battle.BattleActivity;
import com.zarokima.mistwalkers.monsters.MonsterA;
import com.zarokima.mistwalkers.monsters.MonsterB;
import com.zarokima.mistwalkers.monsters.MonsterC;
import com.zarokima.mistwalkers.monsters.Party;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.graphics.PointF;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.widget.TextView;

public class MapMonsterNPC extends NonPlayerCharacter
{
	private static Random random = new Random();
	private Scene scene = null;
	private Party party;

	public MapMonsterNPC(Context c, BitmapTextureAtlas bta, String path, String n, Scene s)
	{
		super(c, bta, path, n);
		monsterSetup(s);
		// TODO Auto-generated constructor stub
	}

	public MapMonsterNPC(Context c, BitmapTextureAtlas bta, String path, Scene s)
	{
		super(c, bta, path);
		monsterSetup(s);
		// TODO Auto-generated constructor stub
	}

	public MapMonsterNPC(Context c, PointF p, BitmapTextureAtlas bta, String path, String n, Scene s)
	{
		super(c, p, bta, path, n);
		monsterSetup(s);
		// TODO Auto-generated constructor stub
	}

	public MapMonsterNPC(Context c, PointF p, BitmapTextureAtlas bta, String path, Scene s)
	{
		super(c, p, bta, path);
		monsterSetup(s);
		// TODO Auto-generated constructor stub
	}

	private void monsterSetup(Scene s)
	{
		scene = s;
		speakBehaviors.add(new MonsterSpeak("Monster!", "You can't fight me yet, but let's just say you won."));
		party = new Party(new MonsterA(), new MonsterB(), new MonsterC());
		Random rand = new Random();
		party.getMonster1().addXP(rand.nextInt(500));
		party.getMonster2().addXP(rand.nextInt(500));
		party.getMonster3().addXP(rand.nextInt(500));
	}

	@Override
	public void onUpdate(float dt)
	{
		super.onUpdate(dt);

		if (A_path == null)
		{
			try
			{
				Direction direction = Direction.values()[random.nextInt(Direction.values().length)];
				float[] coords;
				switch (direction)
				{
					case UP:
						coords = scene.convertLocalToSceneCoordinates(locationTile.getTileX(), locationTile.getTileY()
								- ExploreActivity.TILE_HEIGHT);
						walkTo(coords[0], coords[1], scene);
						break;
					case DOWN:
						coords = scene.convertLocalToSceneCoordinates(locationTile.getTileX(), locationTile.getTileY()
								+ ExploreActivity.TILE_HEIGHT);
						walkTo(coords[0], coords[1], scene);
						break;
					case LEFT:
						coords = scene.convertLocalToSceneCoordinates(locationTile.getTileX()
								- ExploreActivity.TILE_WIDTH, locationTile.getTileY());
						walkTo(coords[0], coords[1], scene);
						break;
					case RIGHT:
						coords = scene.convertLocalToSceneCoordinates(locationTile.getTileX()
								+ ExploreActivity.TILE_WIDTH, locationTile.getTileY());
						walkTo(coords[0], coords[1], scene);
						break;
				}
			}
			catch (Exception e)
			{
				// just stand still for this step
			}
		}
	}

	private class MonsterSpeak extends SpeakBehavior
	{

		public MonsterSpeak(String title, String message)
		{
			super(title, message);
		}

		@Override
		protected void speak(Activity activity, Context context, final ArrayList<SpeakBehavior> behaviors)
		{
			sprite.setPosition(0, 0);
			sprite.detachSelf();
			Intent intent = new Intent(context, BattleActivity.class);
			intent.putExtra("PlayerParty", ExploreActivity.player.getParty());
			intent.putExtra("EnemyParty", party);
			intent.putExtra("Inventory", ExploreActivity.player.getInventory());
			activity.startActivity(intent);
		}
	}
}
