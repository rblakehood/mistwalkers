package com.zarokima.mistwalkers.explore;

import java.io.IOException;
import java.util.ArrayList;
import org.anddev.andengine.audio.music.Music;
import org.anddev.andengine.audio.music.MusicFactory;
import org.anddev.andengine.engine.Engine;
import org.anddev.andengine.engine.camera.Camera;
import org.anddev.andengine.engine.options.EngineOptions;
import org.anddev.andengine.engine.options.EngineOptions.ScreenOrientation;
import org.anddev.andengine.engine.options.resolutionpolicy.FillResolutionPolicy;
import org.anddev.andengine.entity.primitive.Rectangle;
import org.anddev.andengine.entity.scene.Scene;
import org.anddev.andengine.entity.scene.Scene.IOnSceneTouchListener;
import org.anddev.andengine.entity.scene.background.ColorBackground;
import org.anddev.andengine.entity.sprite.Sprite;
import org.anddev.andengine.entity.text.Text;
import org.anddev.andengine.entity.util.FPSLogger;
import org.anddev.andengine.input.touch.TouchEvent;
import org.anddev.andengine.input.touch.detector.ScrollDetector;
import org.anddev.andengine.input.touch.detector.ScrollDetector.IScrollDetectorListener;
import org.anddev.andengine.input.touch.detector.SurfaceScrollDetector;
import org.anddev.andengine.opengl.font.Font;
import org.anddev.andengine.opengl.texture.TextureOptions;
import org.anddev.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.anddev.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.anddev.andengine.opengl.texture.region.TextureRegion;
import org.anddev.andengine.ui.activity.BaseGameActivity;
import org.anddev.andengine.util.Debug;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import com.zarokima.mistwalkers.items.Inventory;
import com.zarokima.mistwalkers.monsters.Party;

public class InventoryActivity extends BaseGameActivity implements IScrollDetectorListener, IOnSceneTouchListener
{

	// ===========================================================
	// Constants
	// ===========================================================
	protected static int CAMERA_WIDTH = 640;
	protected static int CAMERA_HEIGHT = 320;

	protected static int FONT_SIZE = 24;
	protected static int PADDING = 20;
	private static final String MUSIC_PATH = "happyland.ogg";

	// ===========================================================
	// Fields
	// ===========================================================
	private Scene scene;
	private Camera camera;

	private Font font;
	private BitmapTextureAtlas fontTexture;
	private Music music;
	private TextureRegion frameTextureRegion;

	// Scrolling
	private SurfaceScrollDetector scrollDetector;

	private float mMinX = 0;
	private float mMaxX = 0;
	private float mCurrentX = 0;
	private Inventory inventory;
	private Party party;

	private Rectangle scrollBar;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		Bundle extras = getIntent().getExtras();
		if (extras == null)
		{
			Log.d("STATUS", "Intent has no extras");
			this.finish();
		}

		try
		{
			inventory = (Inventory) extras.getSerializable("Inventory");
			party = (Party) extras.getSerializable("Party");
		}
		catch (Exception e)
		{
			Log.d("STATUS", "Something's wrong with the bundle");
			this.finish();
		}
	}

	@Override
	public void onLoadResources()
	{

		// Font
		this.fontTexture = new BitmapTextureAtlas(256, 256);
		this.font = new Font(this.fontTexture, Typeface.create(Typeface.DEFAULT, Typeface.BOLD), FONT_SIZE, true,
				Color.BLACK);
		this.mEngine.getTextureManager().loadTextures(this.fontTexture);
		this.mEngine.getFontManager().loadFonts(this.font);

		BitmapTextureAtlas bta = new BitmapTextureAtlas(256, 512, TextureOptions.DEFAULT);
		frameTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(bta, this.getApplicationContext(),
				"gfx/frames/itemFrame.png", 0, 0);
		this.mEngine.getTextureManager().loadTexture(bta);

		MusicFactory.setAssetBasePath("mfx/");
		try
		{
			music = MusicFactory.createMusicFromAsset(this.mEngine.getMusicManager(), this, MUSIC_PATH);
			music.setLooping(true);
		}
		catch (final IOException e)
		{
			Debug.e(e);
		}
	}

	@Override
	public Engine onLoadEngine()
	{
		this.camera = new Camera(0, 0, CAMERA_WIDTH, CAMERA_HEIGHT);

		final EngineOptions eo = new EngineOptions(true, ScreenOrientation.LANDSCAPE,
				new FillResolutionPolicy(), this.camera);
		eo.getTouchOptions().setRunOnUpdateThread(true);
		eo.setNeedsMusic(true);
		return new Engine(eo);
	}

	@Override
	public Scene onLoadScene()
	{
		this.mEngine.registerUpdateHandler(new FPSLogger());

		this.scene = new Scene();
		this.scene.setBackground(new ColorBackground(1, 1, 1));

		this.scrollDetector = new SurfaceScrollDetector(this);

		this.scene.setOnSceneTouchListener(this);
		this.scene.setTouchAreaBindingEnabled(true);
		this.scene.setOnSceneTouchListenerBindingEnabled(true);

		createMenuBoxes();

		return this.scene;

	}

	@Override
	public boolean onSceneTouchEvent(final Scene pScene, final TouchEvent pSceneTouchEvent)
	{
		this.scrollDetector.onTouchEvent(pSceneTouchEvent);
		return true;
	}

	@Override
	public void onScroll(final ScrollDetector pScollDetector, final TouchEvent pTouchEvent, final float pDistanceX,
			final float pDistanceY)
	{
		// Return if ends are reached
		if (((mCurrentX - pDistanceX) < mMinX))
		{
			return;
		}
		else if ((mCurrentX - pDistanceX) > mMaxX)
		{
			return;
		}

		// Center camera to the current point
		this.camera.offsetCenter(-pDistanceX, 0);
		mCurrentX -= pDistanceX;

		// Set the scrollbar with the camera
		float tempX = camera.getCenterX() - CAMERA_WIDTH / 2;
		// add the % part to the position
		tempX += (tempX / (mMaxX + CAMERA_WIDTH)) * CAMERA_WIDTH;
		// set the position
		scrollBar.setPosition(tempX, scrollBar.getY());

		// Because Camera can have negativ X values, so set to 0
		if (this.camera.getMinX() < 0)
		{
			this.camera.offsetCenter(0, 0);
			mCurrentX = 0;
		}

	}

	private void createMenuBoxes()
	{
		scene.clearTouchAreas();
		scene.detachChildren();
		int spriteX = PADDING;
		int spriteY = 0;
		int itemWidth = 150;

		for (int i = 0; i < inventory.size(); i++)
		{
			final int itemIndex = i;

			Sprite frame = new Sprite(spriteX, spriteY, frameTextureRegion);

			BitmapTextureAtlas bta = new BitmapTextureAtlas(16, 16, TextureOptions.DEFAULT);
			TextureRegion tr = BitmapTextureAtlasTextureRegionFactory.createFromAsset(bta,
					this.getApplicationContext(), inventory.get(i).getImagePath(), 0, 0);
			Sprite sprite = new Sprite(spriteX + 3, 4, tr);
			this.mEngine.getTextureManager().loadTexture(bta);

			bta = new BitmapTextureAtlas(128, 64, TextureOptions.DEFAULT);
			tr = BitmapTextureAtlasTextureRegionFactory.createFromAsset(bta, this.getApplicationContext(),
					"gfx/frames/useItem.png", 0, 0);
			Sprite useSprite = new Sprite(spriteX + 1, 259, tr)
			{
				public boolean onAreaTouched(final TouchEvent event, final float x, final float y)
				{
					if (event.isActionDown())
					{
						useItem(itemIndex); // must be final, so can't just use i
						return true;
					}
					
					return false;
				}
			};;
			this.mEngine.getTextureManager().loadTexture(bta);

			bta = new BitmapTextureAtlas(128, 64, TextureOptions.DEFAULT);
			tr = BitmapTextureAtlasTextureRegionFactory.createFromAsset(bta, this.getApplicationContext(),
					"gfx/frames/dumpItem.png", 0, 0);
			Sprite dumpSprite = new Sprite(spriteX + 81, 259, tr)
			{
				public boolean onAreaTouched(final TouchEvent event, final float x, final float y)
				{
					if (event.isActionDown())
					{
						dumpItem(itemIndex); // must be final, so can't just use i
						return true;
					}
					
					return false;
				}
			};;
			this.mEngine.getTextureManager().loadTexture(bta);

			Text nameText = new Text(spriteX + 3, 20, font, TextHelper.getNormalizedText(font, inventory.get(i)
					.getName(), itemWidth));

			Text descriptionText = new Text(spriteX + 3, 100, font, TextHelper.getNormalizedText(font, inventory.get(i)
					.getDescription(), itemWidth));

			scene.attachChild(frame);
			scene.attachChild(nameText);
			scene.attachChild(descriptionText);
			scene.attachChild(sprite);
			scene.attachChild(useSprite);
			scene.attachChild(dumpSprite);

			scene.registerTouchArea(useSprite);
			scene.registerTouchArea(dumpSprite);

			spriteX += PADDING + frame.getWidth();
		}

		mMaxX = spriteX - CAMERA_WIDTH;

		// set the size of the scrollbar
		float scrollbarsize = CAMERA_WIDTH / ((mMaxX + CAMERA_WIDTH) / CAMERA_WIDTH);
		scrollBar = new Rectangle(mCurrentX, CAMERA_HEIGHT - 20, scrollbarsize, 20);
		scrollBar.setColor(1, 0, 0);
		this.scene.attachChild(scrollBar);
	}

	@Override
	public void onLoadComplete()
	{

	}

	private void useItem(final int index)
	{
		ArrayList<SpeakBehavior> behaviors = new ArrayList<SpeakBehavior>();
		SpeakBehavior sb = new UseItemBehavior(inventory.get(index).getName(), "Use on who?.", index);
		behaviors.add(sb);
		sb.doBehavior(this, this, behaviors);
	}

	private void dumpItem(final int index)
	{
		inventory.removeItem(index);
		createMenuBoxes();
	}
	
	private class UseItemBehavior extends SpeakBehavior
	{
		int itemIndex;

		public UseItemBehavior(String title, String message, int i)
		{
			super(title, message);
			itemIndex = i;
		}
		
		@Override
		protected void speak(final Activity activity, final Context context, final ArrayList<SpeakBehavior> behaviors)
		{
			AlertDialog.Builder builder = new AlertDialog.Builder(activity);
			builder.setTitle(title);
			
			final String[] names = new String[]{party.getMonster1().getName(), party.getMonster2().getName(), party.getMonster3().getName()};
			
			builder.setItems(names, new DialogInterface.OnClickListener()
			{
				@Override
				public void onClick(DialogInterface dialog, int which)
				{
					if(which == 0)
						inventory.useItem(itemIndex, party.getMonster1());
					else if(which == 1)
						inventory.useItem(itemIndex, party.getMonster2());
					else
						inventory.useItem(itemIndex, party.getMonster3());

					dialog.dismiss();
				}
			});
			
			AlertDialog alert = builder.create();
			alert.setOnDismissListener(new OnDismissListener()
			{

				@Override
				public void onDismiss(DialogInterface dialog)
				{
					int index = behaviors.indexOf(UseItemBehavior.this);
					if (index < (behaviors.size() - 1))
					{
						behaviors.get(index + 1).doBehavior(activity, context, behaviors);
					}
					createMenuBoxes();
				}
			});
			alert.show();
		}
		
		protected void setTarget(int which)
		{
			
		}
	}
}
