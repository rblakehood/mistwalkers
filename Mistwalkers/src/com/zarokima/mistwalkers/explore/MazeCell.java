package com.zarokima.mistwalkers.explore;

import java.io.Serializable;
import java.util.ArrayList;
import org.anddev.andengine.util.path.Direction;

public class MazeCell implements Serializable
{	
	private MazeCell[] neighbors;
	private boolean[] checked;
	private boolean inMaze = false;
	private SerialPoint position;
	private static int xMax = 10, yMax = 10; //exclusive boundary for position
	private int mapIndex; //will be used when maze generation is working properly
	private String symbol;
	
	public MazeCell(int x, int y)
	{
		position = new SerialPoint(x,y);
		neighbors = new MazeCell[4];
		checked = new boolean[4];
		for(int i = 0; i < neighbors.length; i++)
		{
			neighbors[i] = null;
		}
	}
	
	public SerialPoint getPosition()
	{
		return position;
	}
	
	public int getMapIndex()
	{
		return mapIndex;
	}
	
	public void setInMaze(boolean b)
	{
		inMaze = b;
	}
	
	public static void setBounds(int x, int y)
	{
		xMax = x;
		yMax = y;
	}
	
	public void setNeighbor(MazeCell c, Direction d)
	{
		checked[d.ordinal()] = true;
		neighbors[d.ordinal()] = c;
	}
	
	public void setDirectionChecked(Direction d, boolean b)
	{
		checked[d.ordinal()] = b;
	}
	
	public boolean hasNeighbor(Direction d)
	{
		return (neighbors[d.ordinal()] != null);
	}
	
	public MazeCell getNeighbor(Direction d)
	{
		return neighbors[d.ordinal()];
	}
	
	public boolean isInMaze()
	{
		return inMaze;
	}

	public Direction[] getUncheckedDirections()
	{
		ArrayList<Direction> al = new ArrayList<Direction>();
		
		for(Direction d : Direction.values())
		{
			//boundary cases
			switch(d)
			{
				case UP:
					if(position.y == 0)
						continue;
					break;
				case DOWN:
					if(position.y == yMax-1)
						continue;
					break;
				case LEFT:
					if(position.x == 0)
						continue;
					break;
				case RIGHT:
					if(position.x == xMax-1)
						continue;
					break;
			}
			if(checked[d.ordinal()] == false)
				al.add(d);
		}
		
		Direction[] d = new Direction[al.size()];
		for(int i = 0; i < d.length; i++)
			d[i] = al.get(i);
		
		return d;
	}
	
	public String getSymbol()
	{
		return symbol;
	}
	
	public void determineSymbol()
	{
		boolean up = false, down = false, right = false, left = false;
		
		for(Direction d : Direction.values())
		{
			if(neighbors[d.ordinal()] != null)
			{
				switch(d)
				{
					case UP:
						up = true;
						break;
					case DOWN:
						down = true;
						break;
					case LEFT:
						left = true;
						break;
					case RIGHT:
						right = true;
						break;
				}
			}
		}
		
		//all characters are from the unicode Box Drawing Block
		//they are all HEAVY, and correspond to every possible path this cell could contain
		//the character encoding is used rather than the characters themselves because things mess up if
		//the file encoding changes with the raw characters. HEAVY VERTICAL AND HORIZONTAL becomes â•‹ for instance
		if(up) //u
		{
			if(down) //ud
			{
				if(left) //udl
				{
					if(right) //udlr
					{
						symbol = "\u254B";
						mapIndex = 105;
					}
					else //udl
					{
						symbol = "\u252B";
						mapIndex = 106;
					}
				}
				else if(right) //udr
				{
					symbol = "\u2523";
					mapIndex = 104;
				}
				else //ud
				{
					symbol = "\u2503";
					mapIndex = 100;
				}
			}
			else if(left) //ul
			{
				if(right) //ulr
				{
					symbol = "\u253B";
					mapIndex = 102;
				}
				else //ul
				{
					symbol = "\u251B";
					mapIndex = 103;
				}
			}
			else if (right) //ur
			{
				symbol = "\u2517";
				mapIndex = 101;
			}
			else //u
			{
				symbol = "\u2579";
				mapIndex = 112;
			}
		}
		else if(down) //d
		{
			if(left) //dl
			{
				if(right) //dlr
				{
					symbol = "\u2533";
					mapIndex = 108;
				}
				else //dl
				{
					symbol = "\u2513";
					mapIndex = 109;
				}
			}
			else if(right) //dr
			{
				symbol = "\u250F";
				mapIndex = 107;
			}
			else //d
			{
				symbol = "\u257B";
				mapIndex = 118;
			}
		}
		else if (left) //l
		{
			if(right) //lr
			{
				symbol = "\u2501";
				mapIndex = 110;
			}
			else //l
			{
				symbol = "\u2578";
				mapIndex = 116;
			}
		}
		else //r
		{
			symbol = "\u257A";
			mapIndex = 114;
		}
	}
}
