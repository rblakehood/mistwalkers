package com.zarokima.mistwalkers.explore;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import org.anddev.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.graphics.PointF;
import android.os.Environment;
import android.util.Log;
import com.zarokima.mistwalkers.scan.ScanActivity;

public class ExplorePortal extends NonPlayerCharacter
{
	private MazeGenerator maze = null;
	private int width = 10;
	private int height = 10;

	public ExplorePortal(Context c, BitmapTextureAtlas bta, String path, String n)
	{
		super(c, bta, path, n);
		portalSetup();
	}

	public ExplorePortal(Context c, PointF p, BitmapTextureAtlas bta, String path, String n)
	{
		super(c, p, bta, path, n);
		portalSetup();
	}

	public MazeGenerator getMazeGenerator()
	{
		return maze;
	}

	public void setSize(int w, int h)
	{
		width = w;
		height = h;
	}

	public long getSeed()
	{
		return maze.getSeed();
	}
	
	private void portalSetup()
	{
		sprite.animate(ANIMATE_DURATION, 0, 2, true);

		Log.d(name, "Attempting to read file");
		try
		{
			Log.d(name, "Attempting to read file");
			BufferedReader buf = new BufferedReader(new InputStreamReader(context.openFileInput(name)));
			long seed = Long.parseLong(buf.readLine());
			generateMaze(seed);
		}
		catch (FileNotFoundException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (NumberFormatException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void interact()
	{
		if (interactOnce)
			return;
		determineBehavior();
		super.interact();
	}

	public void determineBehavior()
	{
		speakBehaviors = new ArrayList<SpeakBehavior>();

		final DialogInterface.OnClickListener walkDownListener = new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				dialog.dismiss();

				ExploreActivity.player.walkTo(sprite.getX(), sprite.getY() + ExploreActivity.TILE_HEIGHT, scene);
			}
		};

		final DialogInterface.OnClickListener generateMazeListener = new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				speakBehaviors.add(new SelectSeedBehavior("Select a UPC to use", "Select a UPC to use."));

				dialog.dismiss();
			}
		};

		if (maze == null)
		{
			speakBehaviors.add(new YesNoBehavior(name, "No maze found. Generate one?", generateMazeListener,
					walkDownListener));
		}
		else
		{
			DialogInterface.OnClickListener showMazeListener = new DialogInterface.OnClickListener()
			{

				@Override
				public void onClick(DialogInterface dialog, int which)
				{
					MapHandler.setCurrentMaze(maze);
					MapHandler.setCurrentCell(maze.getStartCell());
					MapHandler.addExitTile(ExplorePortal.this.getLocationTile(), maze.getStartIndex());
				}
			};

			DialogInterface.OnClickListener regenListener = new DialogInterface.OnClickListener()
			{
				@Override
				public void onClick(DialogInterface dialog, int which)
				{
					speakBehaviors.add(new YesNoBehavior(name, "Generate a new one?", generateMazeListener,
							walkDownListener));
				}
			};

			speakBehaviors.add(new YesNoBehavior(name, "Maze found! Enter maze?", showMazeListener, regenListener));
		}
	}

	private void generateMaze(long seed)
	{
		maze = new MazeGenerator(width, height);
		maze.setSeed(seed);
		maze.generateMaze();

		SpeakBehavior speakMaze = new SpeakBehavior(name, maze.outputMazeText());
		speakMaze.setFont(ExploreActivity.font);
		speakMaze.setFontSize(40);
		speakBehaviors.add(speakMaze);

		final DialogInterface.OnClickListener walkDownListener = new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				dialog.dismiss();

				ExploreActivity.player.walkTo(sprite.getX(), sprite.getY() + ExploreActivity.TILE_HEIGHT, scene);
			}
		};

		DialogInterface.OnClickListener showMazeListener = new DialogInterface.OnClickListener()
		{

			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				MapHandler.setCurrentMaze(maze);
				MapHandler.setCurrentCell(maze.getStartCell());
				MapHandler.addExitTile(ExplorePortal.this.getLocationTile(), maze.getStartIndex());
			}
		};

		speakBehaviors.add(new YesNoBehavior(name, "Enter the maze?", showMazeListener, walkDownListener));
		
		//save seed
		OutputStreamWriter osw;
		try
		{
			osw = new OutputStreamWriter(context.openFileOutput(name, Context.MODE_PRIVATE));
			osw.write(seed + "");
			osw.close();
		}
		catch (FileNotFoundException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private class SelectSeedBehavior extends SpeakBehavior
	{

		public SelectSeedBehavior(String title, String message)
		{
			super(title, message);
		}

		@Override
		protected void speak(final Activity activity, final Context context, final ArrayList<SpeakBehavior> behaviors)
		{
			try
			{
				ArrayList<String> barcodes = new ArrayList<String>();
				
				File file = new File(Environment.getExternalStorageDirectory().toString(), ScanActivity.BARCODE_FILE_PATH);

				if(!file.exists())
	        	{
	        		Log.d("ExplorePortal", "Barcode file does not exist!");
	        		return;
	        	}
				
				BufferedReader buf = new BufferedReader(new FileReader(file));
				String input = buf.readLine();
				while (input != null)
				{
					barcodes.add(input);
					input = buf.readLine();
				}
				buf.close();

				AlertDialog.Builder builder = new AlertDialog.Builder(activity);
				builder.setTitle(title);

				final String[] barcodeArray = barcodes.toArray(new String[0]);

				builder.setItems(barcodeArray, new DialogInterface.OnClickListener()
				{

					@Override
					public void onClick(DialogInterface dialog, int which)
					{
						generateMaze(Long.parseLong(barcodeArray[which]));

						dialog.dismiss();
					}
				});
				AlertDialog alert = builder.create();
				alert.setOnDismissListener(new OnDismissListener()
				{

					@Override
					public void onDismiss(DialogInterface dialog)
					{
						int index = behaviors.indexOf(SelectSeedBehavior.this);
						Log.d("ALERT", "index " + index + " of " + behaviors.size());
						if (index < (behaviors.size() - 1))
						{
							behaviors.get(index + 1).doBehavior(activity, context, behaviors);
						}
					}
				});
				alert.show();
			}
			catch (FileNotFoundException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			catch (IOException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
