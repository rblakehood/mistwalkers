package com.zarokima.mistwalkers.explore;

import org.anddev.andengine.engine.handler.IUpdateHandler;
import org.anddev.andengine.entity.IEntity;
import org.anddev.andengine.entity.layer.tiled.tmx.TMXLayer;
import org.anddev.andengine.entity.layer.tiled.tmx.TMXTile;
import org.anddev.andengine.entity.modifier.IEntityModifier;
import org.anddev.andengine.entity.modifier.IEntityModifier.IEntityModifierListener;
import org.anddev.andengine.entity.modifier.PathModifier;
import org.anddev.andengine.entity.modifier.PathModifier.IPathModifierListener;
import org.anddev.andengine.entity.modifier.PathModifier.Path;
import org.anddev.andengine.entity.scene.Scene;
import org.anddev.andengine.entity.sprite.AnimatedSprite;
import org.anddev.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.anddev.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.anddev.andengine.opengl.texture.region.TiledTextureRegion;
import org.anddev.andengine.util.constants.Constants;
import org.anddev.andengine.util.modifier.IModifier;
import org.anddev.andengine.util.modifier.ease.EaseLinear;
import org.anddev.andengine.util.path.Direction;
import org.anddev.andengine.util.path.ITiledMap;
import org.anddev.andengine.util.path.astar.AStarPathFinder;
import android.content.Context;
import android.graphics.PointF;

//represents any type of living creature within the game world
public abstract class Character implements IUpdateHandler
{
	private static final String TAG = Character.class.getSimpleName();
	protected long[] ANIMATE_DURATION = new long[] { 200, 200, 200 };

	protected AnimatedSprite sprite;
	protected BitmapTextureAtlas textureAtlas;
	protected TiledTextureRegion textureRegion;
	protected float speed = (float) 0.35;
	protected Context context;
	protected Path mCurrentPath;
	protected boolean isWalking;
	protected int mWaypointIndex;
	protected PathModifier mMoveModifier;
	protected IEntityModifier mPathTemp;
	protected org.anddev.andengine.util.path.Path A_path;
	protected AStarPathFinder<TMXLayer> finder;
	protected TMXTile locationTile;
	protected String name;
	protected Direction lastDirection;
	protected static ExploreActivity activity;

	// ==============
	// |Constructors|
	// ==============
	public Character(Context c, PointF p, BitmapTextureAtlas bta, String path, String n)
	{
		context = c;
		textureAtlas = bta;
		textureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(textureAtlas, context, path, 0, 0, 3, 4);
		sprite = new AnimatedSprite(p.x - (textureRegion.getTileWidth() / 2), p.y - (textureRegion.getTileHeight() / 2), textureRegion);
		name = n;
	}

	public Character(Context c, BitmapTextureAtlas bta, String path, String n)
	{
		this(c, new PointF(bta.getWidth() / 2, bta.getHeight() / 2), bta, path, n);
	}
	
	public Character(Context c, PointF p, BitmapTextureAtlas bta, String path)
	{
		this(c, p, bta, path, "Nameless");
	}
	
	public Character(Context c, BitmapTextureAtlas bta, String path)
	{
		this(c, new PointF(bta.getWidth() / 2, bta.getHeight() / 2), bta, path);
	}

	// =========
	// |Getters|
	// =========
	public AnimatedSprite getSprite()
	{
		return sprite;
	}

	public PointF getPosition()
	{
		return new PointF(sprite.getX(), sprite.getY());
	}

	public BitmapTextureAtlas getTextureAtlas()
	{
		return textureAtlas;
	}

	public TiledTextureRegion getTextureRegion()
	{
		return textureRegion;
	}
	
	public TMXTile getLocationTile()
	{
		return locationTile;
	}
	
	public String getName()
	{
		return name;
	}

	// =========
	// |Setters|
	// =========
	public void setTextureAtlas(BitmapTextureAtlas textureAtlas)
	{
		this.textureAtlas = textureAtlas;
	}
	
	public static void setActivity(ExploreActivity ea)
	{
		activity = ea;
	}

	public void setTiledTextureRegion(TiledTextureRegion textureRegion)
	{
		this.textureRegion = textureRegion;
	}

	public void setPosition(float x, float y)
	{
		sprite.setPosition(x, y);
	}
	
	//changes which direction the character faces
	public void setFacing(Direction d)
	{
		switch(d)
		{
			case UP:
				sprite.setCurrentTileIndex(1);
				break;

			case DOWN:
				sprite.setCurrentTileIndex(7);
				break;

			case LEFT:
				sprite.setCurrentTileIndex(10);
				break;

			case RIGHT:
				sprite.setCurrentTileIndex(4);
				break;

			default:
				break;
		}
	}

	@Override
	public void onUpdate(float dt)
	{
		if (MapHandler.isUpdateTiles())
		{
			float[] footCordinates = sprite.convertLocalToSceneCoordinates(8, 8);

			// Get the tile where the center of the current tile is.
			locationTile = MapHandler.getMapLayer().getTMXTileAt(footCordinates[Constants.VERTEX_INDEX_X],
					footCordinates[Constants.VERTEX_INDEX_Y]);
		}
	}

	@Override
	public void reset()
	{
		// TODO Auto-generated method stub
	}

	// ==========
	// |Movement|
	// ==========
	public void setup()
	{
		// These must be defined for the findpath() method to work
		final ITiledMap<TMXLayer> mTiledMap = new ITiledMap<TMXLayer>()
		{

			// Pretty self explanatory
			@Override
			public int getTileColumns()
			{
				return MapHandler.getMap().getTileColumns();
			}

			// Pretty self explanatory
			@Override
			public int getTileRows()
			{
				return MapHandler.getMap().getTileRows();
			}

			// Lets you customize what blocks you want to be considered blocked
			@Override
			public boolean isTileBlocked(TMXLayer pTile, final int pToTileColumn, final int pToTileRow)
			{

				// Tile in the A* Path
				TMXTile blocked = MapHandler.getMapLayer().getTMXTile(pToTileColumn, pToTileRow);

				// Returns true if the tile in the A* Path is contained in the
				// Arraylist CollideTiles
				if (MapHandler.getCollideTiles().contains(blocked))
				{
					return true;

				}
				// Return false by default = no tiles blocked
				return false;
			}

			// This is the key function to understand for AStar pathing Returns
			// the cost of the next tile in the path
			@Override
			public float getStepCost(final TMXLayer pTile, final int pFromTileColumn, final int pFromTileRow,
					final int pToTileColumn, final int pToTileRow)
			{
				// grab the first property from a tile at
				// pToTileColumn x pToTileRow
				// TMXProperty cost =
				// TMXMapLayer.getTMXTile(pToTileColumn,
				// pToTileRow).getTMXTileProperties(mTMXTiledMap).get(0);
				// Gets the value of the string
				// return Float.parseFloat(cost.getValue());
				return 0;

			}

			// If the tile is processed by findpath(), any extra
			// code you might want goes here
			@Override
			public void onTileVisitedByPathFinder(int pTileColumn, int pTileRow)
			{
				// Do Nothing
			}
		};

		// Declare the AStarPathFinder
		// First Param: above ITiledMap
		// Second Param: Max Search Depth - Care, if this is too
		// small your program will crash
		// Third Param: allow diagonal movement or not
		// Fourth Param: Heuristics you want to use in the A*
		// algorithm(optional)
		finder = new AStarPathFinder<TMXLayer>(mTiledMap, 30, false);
	}

	//call this method to walk to the tile at the specified coordinates
	public void walkTo(final float pX, final float pY, Scene pScene)
	{
		if(locationTile == null)
			return;

		// If the user is touching the screen Puts the touch events into an
		// array
		final float[] pToTiles = pScene.convertLocalToSceneCoordinates(pX, pY);

		// Gets the tile at the touched location
		final TMXTile tmxTilePlayerTo = MapHandler.getMapLayer().getTMXTileAt(pToTiles[Constants.VERTEX_INDEX_X],
				pToTiles[Constants.VERTEX_INDEX_Y]);

		/*********/
		// if is walking and there is a A_path ******************
		if (isWalking == true && A_path != null)
		{
			walkToNextWayPoint(pX, pY, pScene);
		}
		else if (A_path == null)
		{
			// Sets the A* path from the player location to the touched
			// location.
			A_path = finder.findPath(MapHandler.getMapLayer(), 20,
			// Sprite's initial tile location
					locationTile.getTileColumn(), locationTile.getTileRow(),
					// Sprite's final tile location
					tmxTilePlayerTo.getTileColumn(), tmxTilePlayerTo.getTileRow());

			// The path with the above parameters should be saved
			loadPathFound();
		}
	}

	private void walkToNextWayPoint(final float pX, final float pY, final Scene pScene)
	{

		sprite.unregisterEntityModifier(mMoveModifier);

		// mPathTemp is another global PathModifier
		sprite.unregisterEntityModifier(mPathTemp);

		final Path lPath = (Path) mCurrentPath;
		// create a new path with length 2 from current sprite position to next
		// original path waypoint
		final Path path = new Path(2);
		path.to(sprite.getX(), sprite.getY()).to(lPath.getCoordinatesX()[mWaypointIndex + 1],
				lPath.getCoordinatesY()[mWaypointIndex + 1]);

		// recalculate the speed.TILE_WIDTH is the tmx tile width, use yours
		// Adjust the speed for different control options
		float TileSpeed = path.getLength() * speed / (ExploreActivity.TILE_WIDTH);

		// Create the modifier of this subpath
		mPathTemp = new PathModifier(TileSpeed, path, new IEntityModifierListener()
		{

			public void onModifierStarted(IModifier<IEntity> pModifier, IEntity pItem)
			{

			}

			public void onModifierFinished(IModifier<IEntity> pModifier, IEntity pItem)
			{

			}
		}, new IPathModifierListener()
		{

			public void onPathWaypointStarted(final PathModifier pPathModifier, final IEntity pEntity,
					int pWaypointIndex)
			{

			}

			public void onPathWaypointFinished(PathModifier pPathModifier, IEntity pEntity, int pWaypointIndex)
			{

			}

			public void onPathStarted(PathModifier pPathModifier, IEntity pEntity)
			{

			}

			public void onPathFinished(PathModifier pPathModifier, IEntity pEntity)
			{
				A_path = null;
				sprite.stopAnimation();
				setFacing(lastDirection);
				walkTo(pX, pY, pScene);
			}
		});

		sprite.registerEntityModifier(mPathTemp);
	}

	private void loadPathFound()
	{

		if (A_path != null)
		{
			// Global var
			mCurrentPath = new Path(A_path.getLength());
			int tilewidth = MapHandler.getMap().getTileWidth();
			int tileheight = MapHandler.getMap().getTileHeight();

			for (int i = 0; i < A_path.getLength(); i++)
			{
				mCurrentPath.to(A_path.getTileColumn(i) * tilewidth, A_path.getTileRow(i) * tileheight);
			}
			doPath();
		}
	}

	private void doPath()
	{
		// Create this mMoveModifier as Global, there is TOUCH_SPEED too ->
		// player
		// speed
		mMoveModifier = new PathModifier(speed * A_path.getLength(), mCurrentPath, new IEntityModifierListener()
		{

			public void onModifierStarted(IModifier<IEntity> pModifier, IEntity pItem)
			{

			}

			public void onModifierFinished(IModifier<IEntity> pModifier, IEntity pItem)
			{
				
			}
		}, new PathModifier.IPathModifierListener()
		{

			public void onPathWaypointStarted(final PathModifier pPathModifier, final IEntity pEntity,
					int pWaypointIndex)
			{
				switch (A_path.getDirectionToNextStep(pWaypointIndex))
				{
					case UP:
						sprite.animate(ANIMATE_DURATION, 0, 2, true);
						break;

					case DOWN:
						sprite.animate(ANIMATE_DURATION, 6, 8, true);
						break;

					case LEFT:
						sprite.animate(ANIMATE_DURATION, 9, 11, true);
						break;

					case RIGHT:
						sprite.animate(ANIMATE_DURATION, 3, 5, true);
						break;

					default:
						break;
				}
				lastDirection = A_path.getDirectionToNextStep(pWaypointIndex);
				// Keep the waypointIndex in a Global Var
				mWaypointIndex = pWaypointIndex;

			}

			public void onPathWaypointFinished(PathModifier pPathModifier, IEntity pEntity, int pWaypointIndex)
			{
				
			}

			public void onPathStarted(PathModifier pPathModifier, IEntity pEntity)
			{
				// Set a global var
				isWalking = true;
			}

			public void onPathFinished(PathModifier pPathModifier, IEntity pEntity)
			{
				// Stop walking and set A_path to null
				isWalking = false;
				A_path = null;
				sprite.stopAnimation();
				setFacing(lastDirection);
			}

		}, EaseLinear.getInstance());

		sprite.registerEntityModifier(mMoveModifier);
	}
}
