package com.zarokima.mistwalkers.explore;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import javax.microedition.khronos.opengles.GL10;
import org.anddev.andengine.entity.IEntity;
import org.anddev.andengine.entity.layer.tiled.tmx.TMXLayer;
import org.anddev.andengine.entity.layer.tiled.tmx.TMXObject;
import org.anddev.andengine.entity.layer.tiled.tmx.TMXObjectGroup;
import org.anddev.andengine.entity.layer.tiled.tmx.TMXObjectProperty;
import org.anddev.andengine.entity.layer.tiled.tmx.TMXProperties;
import org.anddev.andengine.entity.layer.tiled.tmx.TMXTile;
import org.anddev.andengine.entity.layer.tiled.tmx.TMXTiledMap;
import org.anddev.andengine.entity.layer.tiled.tmx.util.exception.TMXLoadException;
import org.anddev.andengine.entity.modifier.FadeInModifier;
import org.anddev.andengine.entity.modifier.FadeOutModifier;
import org.anddev.andengine.entity.modifier.IEntityModifier.IEntityModifierListener;
import org.anddev.andengine.opengl.texture.TextureOptions;
import org.anddev.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.anddev.andengine.util.Debug;
import org.anddev.andengine.util.modifier.IModifier;
import org.anddev.andengine.util.modifier.ease.EaseLinear;
import org.anddev.andengine.util.path.Direction;
import android.graphics.PointF;
import android.os.AsyncTask;
import android.util.Log;

// IMPORTANT NOTE FOR MAP CREATION
// "Player" layer property refers to the direction the player walks after coming from that exit.
// 2 is down, 8 is up, 4 is left, 6 is right -- it's laid out like a number pad

//This class is Used primarily for organization
public class MapHandler extends ExploreActivity
{

	// ===========================================================
	// Constants
	// ===========================================================

	// ===========================================================
	// Fields
	// ===========================================================

	// I kept all fields in the game class

	// ===========================================================
	// Constructors
	// ===========================================================

	public MapHandler()
	{
	}

	// ===========================================================
	// Methods for/from SuperClass/Interfaces
	// ===========================================================

	public static TMXTiledMap getMap()
	{
		return map;
	}

	public static TMXLayer getMapLayer()
	{
		return TMXMapLayer;
	}

	public static List<TMXTile> getCollideTiles()
	{
		return CollideTiles;
	}

	public static boolean isUpdateTiles()
	{
		return UpdateTiles;
	}

	public static boolean isLoadingMap()
	{
		return MapIsLoading;
	}

	public static void setCurrentMaze(MazeGenerator m)
	{
		currentMaze = m;
	}

	public static void setCurrentCell(MazeCell m)
	{
		currentCell = m;
	}

	// ===========================================================
	// Methods
	// ===========================================================

	public static void fadeFromBlack(final float posX, final float posY)
	{

		fadeScreen.setPosition(camera.getCenterX() - fadeScreen.getWidth() / 2,
				camera.getCenterY() - fadeScreen.getHeight() / 2);

		/************
		 * THIS IS IMPORTANT************** The number indicates the Tiled layers you have in your map.It needs to be
		 * higher than the amount of layers you have on your scene. It can't be too high either otherwise the screen
		 * will not respond. So far this solution works
		 */
		scene.attachChild(fadeScreen, scene.getChildCount());

		// Sets up the fade properties
		FadeOutModifier prFadeOutModifier = new FadeOutModifier(1f, new IEntityModifierListener()
		{
			@Override
			public void onModifierStarted(IModifier<IEntity> pModifier, IEntity pItem)
			{

				/*
				 * This controls the walking animation after the scene has loaded The reason I used numbers is because
				 * switch statements do not support strings. If you look at your num pad I think it should make sense
				 */
				switch (playerSpawnDirection)
				{
					case 4:
						// LEFT
						player.walkTo(posX - TILE_WIDTH, posY, scene);
						break;

					case 6:
						// RIGHT
						player.walkTo(posX + TILE_WIDTH, posY, scene);
						break;

					case 2:
						// DOWN
						player.walkTo(posX, posY + TILE_HEIGHT, scene);
						break;

					case 8:
						// UP
						player.walkTo(posX, posY - TILE_HEIGHT, scene);
						break;

					default:
						break;
				}
			}

			@Override
			public void onModifierFinished(IModifier<IEntity> pModifier, IEntity pItem)
			{
				// Once the scene has faded to the scene then the map has finished loading
				// See OnSceneTouchEvent and the updateHandler to see what this controls
				MapIsLoading = false;
			}
		}, EaseLinear.getInstance());

		// Apply the fade properties
		fadeScreen.setBlendFunction(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);
		fadeScreen.registerEntityModifier(prFadeOutModifier);
	}

	// ************************************************************************************************************************

	public static void fadeToBlack()
	{

		fadeScreen.setPosition(camera.getCenterX() - fadeScreen.getWidth() / 2,
				camera.getCenterY() - fadeScreen.getHeight() / 2);

		// Sets up the fade properties
		FadeInModifier prFadeInModifier = new FadeInModifier(1f, new IEntityModifierListener()
		{
			@Override
			public void onModifierStarted(IModifier<IEntity> pModifier, IEntity pItem)
			{
			}

			@Override
			public void onModifierFinished(IModifier<IEntity> pModifier, IEntity pItem)
			{
				/*
				 * The first index is the position in the array the Exit tile, at the players feet, is located at.The
				 * second index is the map number associated with the first Index
				 */
				int tempTransIndex = ExitTiles.indexOf(player.getLocationTile());
				int tempMapIndex = MapNumber.get(tempTransIndex);
				if (LinkNumber.get(tempTransIndex) != null)
					linkIndex = LinkNumber.get(tempTransIndex);

				if (neighborCells.size() == ExitTiles.size())
					currentCell = neighborCells.get(tempTransIndex);
				// loads & attaches the assets for the next map
				loadAndAttachAssets(tempMapIndex);

				scene.detachChild(fadeScreen);

			}
		}, EaseLinear.getInstance());

		// Apply the fade properties
		fadeScreen.setBlendFunction(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);
		fadeScreen.registerEntityModifier(prFadeInModifier);

	}

	// *******************************************************************************************************************

	public static void makePropertyTiles()
	{
		propertyTiles = new ArrayList<TMXTile>();
		propertyIndex = new ArrayList<Integer>();
		// The for loop cycles through each object on the map
		for (final TMXObjectGroup group : map.getTMXObjectGroups())
		{

			// Gets the object layer with these properties. Use if you have many object layers, Otherwise this can be
			// removed
			try
			{
				if (group.getTMXObjectGroupProperties().get(0).getName().equals("Property"))
				{

					for (final TMXObject object : group.getTMXObjects())
					{
						ObjectX = object.getX() + TILE_WIDTH / 2;
						ObjectY = object.getY() + TILE_HEIGHT / 2;
						// Gets the number of rows and columns in the object
						ObjectHeight = object.getHeight() / TILE_HEIGHT;
						ObjectWidth = object.getWidth() / TILE_WIDTH;

						// Gets the tiles the object covers and puts it into the Arraylist CollideTiles
						for (int TileRow = 0; TileRow < ObjectHeight; TileRow++)
						{
							for (int TileColumn = 0; TileColumn < ObjectWidth; TileColumn++)
							{
								TMXTile tempTile = TMXMapLayer.getTMXTileAt(ObjectX + TileColumn * TILE_WIDTH, ObjectY
										+ TileRow * TILE_HEIGHT);

								propertyTiles.add(tempTile);
								propertyIndex.add(Integer.parseInt(group.getTMXObjectGroupProperties().get(0)
										.getValue()));
							}
						}

					}
				}
			}
			catch (IndexOutOfBoundsException e)
			{
				// do nothing and continue
			}

		}
	}

	// *********************************************************************************************************************

	public static void makeAnimatedTiles()
	{
		animatedTiles = new ArrayList<TMXTile>();

		for (int TileRow = 0; TileRow < mAnimateLayer.getTileRows(); TileRow++)
		{
			for (int TileColumn = 0; TileColumn < mAnimateLayer.getTileColumns(); TileColumn++)
			{
				TMXTile tempTile = mAnimateLayer.getTMXTileAt(TILE_WIDTH / 2 + TileColumn * TILE_WIDTH, TILE_HEIGHT / 2
						+ TileRow * TILE_HEIGHT);
				if (tempTile != null)
					animatedTiles.add(tempTile);
			}
		}
	}

	// *********************************************************************************************************************

	public static void makeCollideTiles()
	{
		/*
		 * Determines the tiles that have the Object layer
		 */
		// Uses an array list because the size will need to be
		// changed dynamically.
		CollideTiles = new ArrayList<TMXTile>();
		// The for loop cycles through each object on the map
		for (final TMXObjectGroup group : map.getTMXObjectGroups())
		{

			// Gets the object layer with these properties. Use if
			// you have many object layers, Otherwise this can be
			// removed
			try
			{
				if (group.getTMXObjectGroupProperties().containsTMXProperty("Collide", "true"))
				{

					for (final TMXObject object : group.getTMXObjects())
					{

						ObjectX = object.getX() + TILE_WIDTH / 2;
						ObjectY = object.getY() + TILE_HEIGHT / 2;
						// Gets the number of rows and columns in the
						// object
						ObjectHeight = object.getHeight() / TILE_HEIGHT;
						ObjectWidth = object.getWidth() / TILE_WIDTH;

						// Gets the tiles the object covers and puts it
						// into the Arraylist CollideTiles
						for (int TileRow = 0; TileRow < ObjectHeight; TileRow++)
						{
							for (int TileColumn = 0; TileColumn < ObjectWidth; TileColumn++)
							{
								TMXTile tempTile = TMXMapLayer.getTMXTileAt(ObjectX + TileColumn * TILE_WIDTH, ObjectY
										+ TileRow * TILE_HEIGHT);
								CollideTiles.add(tempTile);
							}
						}

					}
				}
			}
			catch (IndexOutOfBoundsException e)
			{
				// do nothing and continue
			}

		}
	}

	// *********************************************************************************************************************

	public static void makePlayerSpawnTiles()
	{
		playerSpawnTiles = new ArrayList<TMXTile>();

		// The for loop cycles through each object on the map
		for (final TMXObjectGroup group : map.getTMXObjectGroups())
		{

			// Gets the object layer with these properties. Use if you have many object layers, Otherwise this can be
			// removed
			try
			{
				if (group.getTMXObjectGroupProperties().containsTMXProperty("Exit", Integer.toString(currentMapIndex)))
				{

					for (final TMXObject object : group.getTMXObjects())
					{
						ObjectX = object.getX() + TILE_WIDTH / 2;
						ObjectY = object.getY() + TILE_HEIGHT / 2;
						// Gets the number of rows and columns in the
						// object
						ObjectHeight = object.getHeight() / TILE_HEIGHT;
						ObjectWidth = object.getWidth() / TILE_WIDTH;

						// Gets the tiles the object covers and puts it into the Arraylist
						for (int TileRow = 0; TileRow < ObjectHeight; TileRow++)
						{
							for (int TileColumn = 0; TileColumn < ObjectWidth; TileColumn++)
							{
								TMXTile tempTile = TMXMapLayer.getTMXTileAt(ObjectX + TileColumn * TILE_WIDTH, ObjectY
										+ TileRow * TILE_HEIGHT);

								// If the tile has a specific link then go to the specific location else go to the only
								// location
								if (group.getTMXObjectGroupProperties().containsTMXProperty("Link",
										Integer.toString(linkIndex)))
								{

									playerSpawnTiles.add(tempTile);
									// The second property needs to be the player direction he will be walking in when
									// he
									// enters the map
									playerSpawnDirection = Integer.parseInt(group.getTMXObjectGroupProperties().get(2)
											.getValue());

								}
								else if (group.getTMXObjectGroupProperties().size() < 3)
								{
									playerSpawnTiles.add(tempTile);
									// This works if there are only two properties
									playerSpawnDirection = Integer.parseInt(group.getTMXObjectGroupProperties().get(1)
											.getValue());

								}
							}
						}
					}
				}
				else if (group.getTMXObjectGroupProperties().containsTMXProperty("Maze Exit",
						oppositeDirectionOf(mazeDirection).toString()))
				{
					for (final TMXObject object : group.getTMXObjects())
					{
						ObjectX = object.getX() + TILE_WIDTH / 2;
						ObjectY = object.getY() + TILE_HEIGHT / 2;
						// Gets the number of rows and columns in the
						// object
						ObjectHeight = object.getHeight() / TILE_HEIGHT;
						ObjectWidth = object.getWidth() / TILE_WIDTH;

						// Gets the tiles the object covers and puts it into the Arraylist
						for (int TileRow = 0; TileRow < ObjectHeight; TileRow++)
						{
							for (int TileColumn = 0; TileColumn < ObjectWidth; TileColumn++)
							{
								TMXTile tempTile = TMXMapLayer.getTMXTileAt(ObjectX + TileColumn * TILE_WIDTH, ObjectY
										+ TileRow * TILE_HEIGHT);

								playerSpawnTiles.add(tempTile);
								// This works if there are only two properties
								playerSpawnDirection = Integer.parseInt(group.getTMXObjectGroupProperties().get(1)
										.getValue());

							}
						}
					}
				}
			}
			catch (IndexOutOfBoundsException e)
			{
				// do nothing and continue
			}

		}
	}

	// *********************************************************************************************************************

	public static void makeExitTiles()
	{
		// Uses an array list because the size will need to be changed dynamically.
		ExitTiles = new ArrayList<TMXTile>();
		MapNumber = new ArrayList<Integer>();
		LinkNumber = new ArrayList<Integer>();
		neighborCells = new ArrayList<MazeCell>();
		neighborCellDirections = new ArrayList<Direction>();
		// The for loop cycles through each object on the map
		for (final TMXObjectGroup group : map.getTMXObjectGroups())
		{

			/*
			 * Gets the object layer with these properties. Use if you have many object layers, Otherwise this can be
			 * removed
			 */
			try
			{
				if (group.getTMXObjectGroupProperties().get(0).getName().equals("Exit"))
				{

					for (final TMXObject object : group.getTMXObjects())
					{

						ObjectX = object.getX() + TILE_WIDTH / 2;
						ObjectY = object.getY() + TILE_HEIGHT / 2;
						// Gets the number of rows and columns in the object
						ObjectHeight = object.getHeight() / TILE_HEIGHT;
						ObjectWidth = object.getWidth() / TILE_WIDTH;

						// Gets the tiles the object covers and puts it into the Arraylist CollideTiles
						for (int TileRow = 0; TileRow < ObjectHeight; TileRow++)
						{
							for (int TileColumn = 0; TileColumn < ObjectWidth; TileColumn++)
							{
								TMXTile tempTile = TMXMapLayer.getTMXTileAt(ObjectX + TileColumn * TILE_WIDTH, ObjectY
										+ TileRow * TILE_HEIGHT);
								ExitTiles.add(tempTile);
								MapNumber.add(Integer.parseInt(group.getTMXObjectGroupProperties().get(0).getValue()
										.toUpperCase()));

								// This means that the group has the link property in it
								if (group.getTMXObjectGroupProperties().size() >= 3)
								{
									LinkNumber.add(Integer.parseInt(group.getTMXObjectGroupProperties().get(1)
											.getValue()));
								}
								else
								{
									// fills the array to match ExitTiles
									LinkNumber.add(null);
								}
							}
						}

					}
				}
				else if (group.getTMXObjectGroupProperties().get(0).getName().equals("Maze Exit"))
				{
					for (final TMXObject object : group.getTMXObjects())
					{

						ObjectX = object.getX() + TILE_WIDTH / 2;
						ObjectY = object.getY() + TILE_HEIGHT / 2;
						// Gets the number of rows and columns in the object
						ObjectHeight = object.getHeight() / TILE_HEIGHT;
						ObjectWidth = object.getWidth() / TILE_WIDTH;

						// Gets the tiles the object covers and puts it into the Arraylist CollideTiles
						for (int TileRow = 0; TileRow < ObjectHeight; TileRow++)
						{
							for (int TileColumn = 0; TileColumn < ObjectWidth; TileColumn++)
							{
								TMXTile tempTile = TMXMapLayer.getTMXTileAt(ObjectX + TileColumn * TILE_WIDTH, ObjectY
										+ TileRow * TILE_HEIGHT);
								ExitTiles.add(tempTile);

								int exitIndex = 13; // go back to the portal room if something went wrong
								Direction direction = Direction.valueOf(group.getTMXObjectGroupProperties().get(0)
										.getValue().toUpperCase());
								MazeCell neighbor = null;
								SerialPoint position = currentCell.getPosition();

								switch (direction)
								{
									case UP:
										neighbor = currentMaze.getCell(position.x, position.y - 1);
										break;
									case DOWN:
										neighbor = currentMaze.getCell(position.x, position.y + 1);
										break;
									case LEFT:
										neighbor = currentMaze.getCell(position.x - 1, position.y);
										break;
									case RIGHT:
										neighbor = currentMaze.getCell(position.x + 1, position.y);
										break;
								}

								exitIndex = neighbor.getMapIndex();

								MapNumber.add(exitIndex);

								LinkNumber.add(null);

								neighborCells.add(neighbor);

								neighborCellDirections.add(direction);
							}
						}

					}
				}
			}
			catch (IndexOutOfBoundsException e)
			{
				// do nothing and continue
			}

		}
	}

	// *********************************************************************************************************************

	public static void addExitTile(TMXTile tile, int mapIndex)
	{
		ExitTiles.add(tile);
		MapNumber.add(mapIndex);
		LinkNumber.add(null);
	}

	// *********************************************************************************************************************

	public static void makeNPCs()
	{
		NPCs = new ArrayList<NonPlayerCharacter>();
		if(currentMapIndex == 13)
		{
			portals = new ArrayList<ExplorePortal>();
		}

		for (final TMXObjectGroup group : map.getTMXObjectGroups())
		{
			try
			{
				if (group.getName().equals("NPCs"))
				{
					for (final TMXObject object : group.getTMXObjects())
					{
						// add the NPC to the list
						loadNPC(object);
					}
				}
			}
			catch (IndexOutOfBoundsException e)
			{
				// do nothing and continue
			}

		}
	}

	// *********************************************************************************************************************

	public static void loadNPC(TMXObject object)
	{
		BitmapTextureAtlas bta = new BitmapTextureAtlas(64, 128, TextureOptions.DEFAULT);
		String name = object.getName();
		TMXProperties<TMXObjectProperty> properties = object.getTMXObjectProperties();
		String spritePath = "gfx/CharacterTemplate.png";
		ArrayList<String> messages = new ArrayList<String>();
		String subclass = null;
		Direction facing = Direction.DOWN;
		int pathEndIndex = -1;
		int mazeSize = 0;
		NonPlayerCharacter npc = null;

		// set properties not dealing with the path
		for (TMXObjectProperty property : properties)
		{
			if (!property.getName().startsWith("aPath") && pathEndIndex < 0)
			{
				pathEndIndex = properties.indexOf(property);
			}
			if (property.getName().equalsIgnoreCase("sprite"))
			{
				spritePath = "gfx/" + property.getValue() + ".png";
			}
			else if (property.getName().startsWith("message"))
			{
				messages.add(property.getValue());
			}
			else if (property.getName().equalsIgnoreCase("facing"))
			{
				switch (Integer.parseInt(property.getValue()))
				{
					case 4:
						facing = Direction.LEFT;
						break;
					case 6:
						facing = Direction.RIGHT;
						break;
					case 8:
						facing = Direction.UP;
						break;
					default:
						facing = Direction.DOWN;
						break;
				}
			}
			else if (property.getName().equalsIgnoreCase("subclass"))
			{
				subclass = property.getValue();
			}
			else if (property.getName().equalsIgnoreCase("maze size"))
			{
				mazeSize = Integer.parseInt(property.getValue());
			}
		}

		// create the NPC
		if (subclass == null)
		{
			npc = new NonPlayerCharacter(context, bta, spritePath, name);
		}
		else if(subclass.equalsIgnoreCase("Monster"))
		{
			spritePath = "gfx/MazeMonster.png";
			npc = new MapMonsterNPC(context, bta, spritePath, name, scene);
		}
		else if (subclass.equalsIgnoreCase("MazeMaker"))
		{
			npc = new NPCMazeMaker(context, bta, spritePath, name);
		}
		else if (subclass.equalsIgnoreCase("ExplorePortal"))
		{
			int index = Integer.parseInt(name.substring("Portal ".length()));
			index -= 1; //names start at 1, indices at 0
			npc = new ExplorePortal(context, bta, spritePath, name);
			((ExplorePortal) npc).setSize(mazeSize, mazeSize);
			portals.add(index, (ExplorePortal) npc);
		}

		for (String message : messages)
		{
			npc.addMessage(message);
		}
		npc.setFacing(facing);
		npc.setPosition(object.getX(), object.getY());

		// now create the path, if they have one
		if (pathEndIndex > 0)
		{

			List<TMXObjectProperty> pathList = properties.subList(0, pathEndIndex);
			PointF[] points = new PointF[pathEndIndex + 1];
			points[0] = npc.getPosition(); // start wherever they're placed on the map
			int currentIndex = 1;
			for (TMXObjectProperty node : pathList)
			{
				int x, y;
				Scanner scan = new Scanner(node.getValue());
				scan.useDelimiter(",");
				x = scan.nextInt() * TILE_WIDTH;
				y = scan.nextInt() * TILE_HEIGHT;
				scan.close();
				points[currentIndex] = new PointF(x, y);
				currentIndex++;
			}
			npc.setPathPoints(points);
		}
		NPCs.add(npc);
	}

	// *********************************************************************************************************************

	public static void checkInteractions()
	{
		for (NonPlayerCharacter npc : NPCs)
		{
			if (player.getLocationTile() == npc.getLocationTile())
			{
				npc.interact();
			}
		}
	}

	// *********************************************************************************************************************

	public static void updateTMXMap()
	{

		if (ExitTiles.contains(player.getLocationTile())
				&& player.getLocationTile().getTileX() % map.getTileWidth() == 0
				&& player.getLocationTile().getTileY() % map.getTileHeight() == 0)
		{

			/*
			 * if on the exit tile, the player won't be able to interact with the screen and the update will stop until
			 * the tmx has loaded. Once the load has finished the player will appear on the new map off of the exit tile
			 * and the MapIsLoading variable will return false
			 */
			MapIsLoading = true;
			UpdateTiles = false;
			// Ends the path at the tile the sprite is on
			player.walkTo(player.getLocationTile().getTileX(), player.getLocationTile().getTileY(), scene);

			// TODO: marker
			if (currentMaze != null)
			{
				try
				{
					int tempTransIndex = ExitTiles.indexOf(player.getLocationTile());

					mazeDirection = neighborCellDirections.get(tempTransIndex);
				}
				catch (Exception e)
				{
					// this is horrible practice, but if it works, fuck it
				}
			}

			// Contains the TMXMap loader in onModifierFinished
			fadeToBlack();

		}
	}

	// ********************************************************************************************************************

	public static void makeTMXMap(int MapIndex)
	{
		Log.d("MAPHANDLER", "making map " + MapIndex);
		switch (MapIndex)
		{
			case 0:
				// town outskirts
				try
				{
					map = tmxLoader.loadFromAsset(context, "tmx/HubOutskirts.tmx");
				}
				catch (final TMXLoadException tmxle)
				{
					Debug.e(tmxle);
				}

				break;

			case 1:
				// left-most house in outskirts
				try
				{
					map = tmxLoader.loadFromAsset(context, "tmx/House1.tmx");
				}
				catch (final TMXLoadException tmxle)
				{
					Debug.e(tmxle);
				}
				break;

			case 2:
				// center house in outskirts
				try
				{
					map = tmxLoader.loadFromAsset(context, "tmx/House2.tmx");
				}
				catch (final TMXLoadException tmxle)
				{
					Debug.e(tmxle);
				}
				break;

			case 3:
				// right-most house in outskirts
				try
				{
					map = tmxLoader.loadFromAsset(context, "tmx/House3.tmx");
				}
				catch (final TMXLoadException tmxle)
				{
					Debug.e(tmxle);
				}
				break;
			case 4:
				// southern part of the hub
				try
				{
					map = tmxLoader.loadFromAsset(context, "tmx/HubSouth.tmx");
				}
				catch (final TMXLoadException tmxle)
				{
					Debug.e(tmxle);
				}
				break;
			case 5:
				// left part of the inn
				try
				{
					map = tmxLoader.loadFromAsset(context, "tmx/HubInnLeft.tmx");
				}
				catch (final TMXLoadException tmxle)
				{
					Debug.e(tmxle);
				}
				break;
			case 6:
				// right part of the inn
				try
				{
					map = tmxLoader.loadFromAsset(context, "tmx/HubInnRight.tmx");
				}
				catch (final TMXLoadException tmxle)
				{
					Debug.e(tmxle);
				}
				break;
			case 7:
				// center part of the inn
				try
				{
					map = tmxLoader.loadFromAsset(context, "tmx/HubInnCenter.tmx");
				}
				catch (final TMXLoadException tmxle)
				{
					Debug.e(tmxle);
				}
				break;
			case 8:
				// the shop
				try
				{
					map = tmxLoader.loadFromAsset(context, "tmx/Shop.tmx");
				}
				catch (final TMXLoadException tmxle)
				{
					Debug.e(tmxle);
				}
				break;
			case 9:
				// the guardhouse
				try
				{
					map = tmxLoader.loadFromAsset(context, "tmx/Guardhouse.tmx");
				}
				catch (final TMXLoadException tmxle)
				{
					Debug.e(tmxle);
				}
				break;
			case 10:
				// the walkway to the castle
				try
				{
					map = tmxLoader.loadFromAsset(context, "tmx/HubToCastle.tmx");
				}
				catch (final TMXLoadException tmxle)
				{
					Debug.e(tmxle);
				}
				break;
			case 11:
				// the entry-room of the castle
				try
				{
					map = tmxLoader.loadFromAsset(context, "tmx/CastleEntrance.tmx");
				}
				catch (final TMXLoadException tmxle)
				{
					Debug.e(tmxle);
				}
				break;
			case 12:
				// the castle throne room
				try
				{
					map = tmxLoader.loadFromAsset(context, "tmx/CastleThroneRoom.tmx");
				}
				catch (final TMXLoadException tmxle)
				{
					Debug.e(tmxle);
				}
				break;
			case 13:
				// the portal room
				try
				{
					map = tmxLoader.loadFromAsset(context, "tmx/CastlePortalRoom.tmx");
				}
				catch (final TMXLoadException tmxle)
				{
					Debug.e(tmxle);
				}
				break;
			case 100:
				try
				{
					map = tmxLoader.loadFromAsset(context, "tmx/MazeStraightUpDown.tmx");
				}
				catch (final TMXLoadException tmxle)
				{
					Debug.e(tmxle);
				}
				break;
			case 101:
				try
				{
					map = tmxLoader.loadFromAsset(context, "tmx/MazeCornerUpRight.tmx");
				}
				catch (final TMXLoadException tmxle)
				{
					Debug.e(tmxle);
				}
				break;
			case 102:
				try
				{
					map = tmxLoader.loadFromAsset(context, "tmx/MazeThreeWayUp.tmx");
				}
				catch (final TMXLoadException tmxle)
				{
					Debug.e(tmxle);
				}
				break;
			case 103:
				try
				{
					map = tmxLoader.loadFromAsset(context, "tmx/MazeCornerUpLeft.tmx");
				}
				catch (final TMXLoadException tmxle)
				{
					Debug.e(tmxle);
				}
				break;
			case 104:
				try
				{
					map = tmxLoader.loadFromAsset(context, "tmx/MazeThreeWayRight.tmx");
				}
				catch (final TMXLoadException tmxle)
				{
					Debug.e(tmxle);
				}
				break;
			case 105:
				try
				{
					map = tmxLoader.loadFromAsset(context, "tmx/MazeFourWay.tmx");
				}
				catch (final TMXLoadException tmxle)
				{
					Debug.e(tmxle);
				}
				break;
			case 106:
				try
				{
					map = tmxLoader.loadFromAsset(context, "tmx/MazeThreeWayLeft.tmx");
				}
				catch (final TMXLoadException tmxle)
				{
					Debug.e(tmxle);
				}
				break;
			case 107:
				try
				{
					map = tmxLoader.loadFromAsset(context, "tmx/MazeCornerDownRight.tmx");
				}
				catch (final TMXLoadException tmxle)
				{
					Debug.e(tmxle);
				}
				break;
			case 108:
				try
				{
					map = tmxLoader.loadFromAsset(context, "tmx/MazeThreeWayDown.tmx");
				}
				catch (final TMXLoadException tmxle)
				{
					Debug.e(tmxle);
				}
				break;
			case 109:
				try
				{
					map = tmxLoader.loadFromAsset(context, "tmx/MazeCornerDownLeft.tmx");
				}
				catch (final TMXLoadException tmxle)
				{
					Debug.e(tmxle);
				}
				break;
			case 110:
				try
				{
					map = tmxLoader.loadFromAsset(context, "tmx/MazeStraightLeftRight.tmx");
				}
				catch (final TMXLoadException tmxle)
				{
					Debug.e(tmxle);
				}
				break;
			case 112:
				try
				{
					map = tmxLoader.loadFromAsset(context, "tmx/MazeEndUp.tmx");
				}
				catch (final TMXLoadException tmxle)
				{
					Debug.e(tmxle);
				}
				break;
			case 114:
				try
				{
					map = tmxLoader.loadFromAsset(context, "tmx/MazeEndRight.tmx");
				}
				catch (final TMXLoadException tmxle)
				{
					Debug.e(tmxle);
				}
				break;
			case 116:
				try
				{
					map = tmxLoader.loadFromAsset(context, "tmx/MazeEndLeft.tmx");
				}
				catch (final TMXLoadException tmxle)
				{
					Debug.e(tmxle);
				}
				break;
			case 118:
				try
				{
					map = tmxLoader.loadFromAsset(context, "tmx/MazeEndDown.tmx");
				}
				catch (final TMXLoadException tmxle)
				{
					Debug.e(tmxle);
				}
				break;
			default:
				break;
		}
	}

	// *********************************************************************************************************************

	public static void loadAndAttachAssets(final int MapIndex)
	{

		// Use AsyncTask to load TMX map from a saved state
		new AsyncTask<Void, Integer, Void>()
		{

			@Override
			protected void onPreExecute()
			{
				// Clears the scene for the next map
				super.onPreExecute();
				scene.clearEntityModifiers();
				scene.detachChildren();
				A_path = null;
				// Clears all layers of their values
				mAnimateLayer = null;
				topLayers = new ArrayList<TMXLayer>();
			}

			// Set what gets done in the background
			@Override
			protected Void doInBackground(Void... params)
			{

				/*
				 * MapIsLoading is needed here because the initial load can be anywhere not just on an exit tile. SEE
				 * updateTMXMap() method
				 */
				MapIsLoading = true;

				// Load the TMX map
				makeTMXMap(MapIndex);

				AssetsAttached = false;

				return null;
			}

			// Set what gets done after doInBackgroung ends
			@Override
			protected void onPostExecute(Void result)
			{
				System.gc();
				AttachAssets();

			}

		}.execute();
	}

	// **********************************************************************************************************************

	public static void AttachAssets()
	{
		/*
		 * Attaches the layers for this map and sets them to be invisible. Explained below.
		 */
		if (!AssetsAttached)
		{
			for (int LayerIndex = 0; LayerIndex < map.getTMXLayers().size(); LayerIndex++)
			{
				TMXLayer layer = map.getTMXLayers().get(LayerIndex);
				if (layer.getTMXLayerProperties().containsTMXProperty("Animate", "true"))
				{
					mAnimateLayer = layer;
				}
				if (layer.getTMXLayerProperties().containsTMXProperty("Top", "true"))
				{
					// This layer is attached later
					layer.setVisible(false);
					topLayers.add(layer);
				}
				else
				{
					layer.setVisible(false);
					scene.attachChild(layer);
				}
			}

			// Layer initialization
			TMXMapLayer = map.getTMXLayers().get(0);

			// Gets the tiles which activate special events
			makeExitTiles();
			makePlayerSpawnTiles();
			makePropertyTiles();
			makeCollideTiles();
			makeNPCs();
			loadNPCs = true;
			if (mAnimateLayer != null)
			{
				makeAnimatedTiles();
			}

			// Gets the spawn location of the player sprite
			float playerX;
			float playerY;
			if (playerSpawnTiles.size() > 0)
			{
				playerX = playerSpawnTiles.get(0).getTileX();
				playerY = playerSpawnTiles.get(0).getTileY();

			}
			else
			{
				playerX = InitialplayerSpawnX;
				playerY = InitialplayerSpawnY;
				playerSpawnDirection = 5;// do not move on spawn
				player.setFacing(Direction.DOWN);
			}

			// add NPCs first so player will be drawn on top of them.
			for (NonPlayerCharacter npc : NPCs)
			{
				scene.attachChild(npc.getSprite());
				npc.setup();
				npc.setScene(scene);
				scene.registerUpdateHandler(npc);
			}

			// Add player sprite to the scene with the chase camera on the player sprite
			// Create the player sprite at the specified location
			player.setPosition(playerX, playerY);
			scene.attachChild(player.getSprite());

			// Attach the top layer, if it is on the map, after the player is added so that the player can travel under
			// it
			for (TMXLayer layer : topLayers)
			{
				scene.attachChild(layer);
			}

			/*
			 * This activates the ability to Update the player location on the tiles. It is used in the UpdateHandler.
			 * This enables the walking effect when a scene changes, since the walkto method requires the players
			 * current tile to make a path
			 */
			UpdateTiles = true;

			// Make the camera not exceed the bounds of the TMXEntity. Sets the Chase entity and Sets the center of the
			// camera
			// camera.setCenter(playerX, playerY);
			camera.setChaseEntity(player.getSprite());
			camera.setBounds(0, TMXMapLayer.getWidth(), 0, TMXMapLayer.getHeight());
			camera.setBoundsEnabled(EnableBounds);

			player.setup();

			// Only attaches the assets once
			AssetsAttached = true;

			/*
			 * Make all layers visible right before fading in. If you know the layers you are using then it may be
			 * easier to just add them manuallyThis is used to prevent the flickering caused by the moving camera. This
			 * also ensures a smooth transition since everything has been loaded
			 */
			for (int LayerIndex = 0; LayerIndex < map.getTMXLayers().size(); LayerIndex++)
			{
				TMXLayer layer = map.getTMXLayers().get(LayerIndex);
				layer.setVisible(true);

			}

			// Calls the fade method. See above for method details.
			fadeFromBlack(playerX, playerY);

			// Removes the detached layers
			System.gc();
		}

	}

	public static Direction oppositeDirectionOf(Direction d)
	{
		Direction out = null;
		switch (d)
		{
			case UP:
				out = Direction.DOWN;
				break;
			case DOWN:
				out = Direction.UP;
				break;
			case LEFT:
				out = Direction.RIGHT;
				break;
			case RIGHT:
				out = Direction.LEFT;
				break;
		}

		return out;
	}

}
