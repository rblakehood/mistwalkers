package com.zarokima.mistwalkers.explore;

import java.io.IOException;
import java.util.ArrayList;
import org.anddev.andengine.audio.music.Music;
import org.anddev.andengine.audio.music.MusicFactory;
import org.anddev.andengine.engine.Engine;
import org.anddev.andengine.engine.camera.BoundCamera;
import org.anddev.andengine.engine.camera.Camera;
import org.anddev.andengine.engine.handler.IUpdateHandler;
import org.anddev.andengine.engine.options.EngineOptions;
import org.anddev.andengine.engine.options.EngineOptions.ScreenOrientation;
import org.anddev.andengine.engine.options.resolutionpolicy.RatioResolutionPolicy;
import org.anddev.andengine.entity.scene.Scene;
import org.anddev.andengine.entity.scene.Scene.IOnSceneTouchListener;
import org.anddev.andengine.entity.scene.background.ColorBackground;
import org.anddev.andengine.entity.sprite.Sprite;
import org.anddev.andengine.entity.text.ChangeableText;
import org.anddev.andengine.entity.text.Text;
import org.anddev.andengine.entity.util.FPSLogger;
import org.anddev.andengine.input.touch.TouchEvent;
import org.anddev.andengine.opengl.font.Font;
import org.anddev.andengine.opengl.texture.TextureOptions;
import org.anddev.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.anddev.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.anddev.andengine.opengl.texture.region.TextureRegion;
import org.anddev.andengine.ui.activity.BaseGameActivity;
import org.anddev.andengine.util.Debug;
import org.anddev.andengine.util.HorizontalAlign;
import com.zarokima.mistwalkers.monsters.Monster;
import com.zarokima.mistwalkers.monsters.Party;
import com.zarokima.mistwalkers.skills.Skill;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;

public class StatusScreenActivity extends BaseGameActivity implements IOnSceneTouchListener
{
	private static final float CAMERA_HEIGHT = 360;
	private static final float CAMERA_WIDTH = 604;
	private static final String MUSIC_PATH = "happyland.ogg";
	private Party party;
	private Scene scene;
	private Camera camera;
	private BitmapTextureAtlas fontTexture;
	private Font font;
	private int currentMonster = 1;
	private int currentSkill = 0;
	private ArrayList<Skill> skills;
	private ArrayList<ChangeableText> skillTextList;
	private Sprite prevSkillSprite;
	private Sprite nextSkillSprite;
	private Music music;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		Bundle extras = getIntent().getExtras();
		if (extras == null)
		{
			Log.d("STATUS", "Intent has no extras");
			this.finish();
		}

		try
		{
			party = (Party) extras.getSerializable("Party");
		}
		catch (Exception e)
		{
			Log.d("STATUS", "Something's wrong with the bundle");
			this.finish();
		}
	}

	@Override
	public Engine onLoadEngine()
	{
		camera = new BoundCamera(0, 0, CAMERA_WIDTH, CAMERA_HEIGHT);
		EngineOptions eo = new EngineOptions(true, ScreenOrientation.LANDSCAPE, new RatioResolutionPolicy(CAMERA_WIDTH,
				CAMERA_HEIGHT), camera);
		eo.getTouchOptions().setRunOnUpdateThread(true);
		eo.setNeedsMusic(true);
		return new Engine(eo);
	}

	@Override
	public void onLoadResources()
	{
		this.fontTexture = new BitmapTextureAtlas(256, 256, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
		this.font = new Font(this.fontTexture, Typeface.create(Typeface.DEFAULT, Typeface.BOLD), 26, true, Color.BLACK);
		this.mEngine.getTextureManager().loadTexture(this.fontTexture);
		this.getFontManager().loadFont(this.font);
		
		MusicFactory.setAssetBasePath("mfx/");
		try
		{
			music = MusicFactory.createMusicFromAsset(this.mEngine.getMusicManager(), this, MUSIC_PATH);
			music.setLooping(true);
		}
		catch (final IOException e)
		{
			Debug.e(e);
		}
	}

	@Override
	public Scene onLoadScene()
	{
		this.mEngine.registerUpdateHandler(new FPSLogger());

		scene = new Scene();
		scene.setBackground(new ColorBackground(1f, 1f, 1f));
		scene.setOnSceneTouchListener(this);

		loadMonster(party.getMonster1());
		
		music.play();

		return scene;
	}

	@Override
	public void onLoadComplete()
	{
	}

	@Override
	public boolean onSceneTouchEvent(Scene sce, TouchEvent event)
	{
		if (!event.isActionDown())
			return true;

		Log.d("STATUS", event.getX() + ", " + event.getY());
		if (event.getX() <= 32 || event.getX() >= 572)
		{
			if (event.getX() <= 32)
			{
				currentMonster--;
				if (currentMonster == 0)
					currentMonster = 3;
			}
			else
			{
				currentMonster++;
				if (currentMonster == 4)
					currentMonster = 1;
			}

			switch (currentMonster)
			{
				case 1:
					loadMonster(party.getMonster1());
					break;
				case 2:
					loadMonster(party.getMonster2());
					break;
				case 3:
					loadMonster(party.getMonster3());
					break;
				default:
					break;
			}
		}
		else if (prevSkillSprite.contains(event.getX(), event.getY())
				|| nextSkillSprite.contains(event.getX(), event.getY()))
		{
			if (prevSkillSprite.contains(event.getX(), event.getY()))
			{
				currentSkill--;
				if (currentSkill == -1)
					currentSkill = skills.size() - 1;
			}
			else
			{
				currentSkill++;
				if (currentSkill == skills.size())
					currentSkill = 0;
			}
			
			changeSkill(currentSkill);
		}

		return true;
	}

	private void loadMonster(Monster monster)
	{
		scene.setVisible(false);
		scene.detachChildren();

		skills = monster.getSkills();
		skillTextList = new ArrayList<ChangeableText>();
		ArrayList<Text> textList = new ArrayList<Text>();

		textList.add(new Text(35, 0, font, monster.getName()));
		textList.add(new Text(35, 32, font, "Level: " + monster.getLevel()));
		textList.add(new Text(35, 64, font, "Exp: " + monster.getCurrentXP()));
		textList.add(new Text(35, 96, font, "To lvl: " + (monster.getXPToLevel() - monster.getCurrentXP())));
		textList.add(new Text(35, 128, font, "HP: " + monster.getCurrentHP() + "/" + monster.getMaxHP()));
		textList.add(new Text(35, 160, font, "MP: " + monster.getCurrentMP() + "/" + monster.getMaxMP()));
		textList.add(new Text(35, 192, font, "P. Att: " + monster.getPhysicalAttack()));
		textList.add(new Text(35, 224, font, "P. Def: " + monster.getPhysicalDefense()));
		textList.add(new Text(35, 256, font, "M. Att: " + monster.getMagicalAttack()));
		textList.add(new Text(35, 288, font, "M. Def: " + monster.getMagicalDefense()));
		textList.add(new Text(35, 320, font, "Agility: " + monster.getAgility()));
		textList.add(new Text(415, 0, font, monster.getSpecies(), HorizontalAlign.CENTER));
		textList.add(new Text(415, 160, font, TextHelper.getNormalizedText(font, monster.getDescription(), 150)));

		skillTextList.add(new ChangeableText(195, 32, font, skills.get(0).getName(), 15));
		skillTextList.add(new ChangeableText(195, 64, font, "Type: " + skills.get(0).getType().toString(), 15));
		skillTextList.add(new ChangeableText(195, 96, font, "Cost: " + skills.get(0).getCost() + "MP", 15));
		skillTextList.add(new ChangeableText(195, 128, font, "Power: " + skills.get(0).getDamage(), 15));
		skillTextList.add(new ChangeableText(195, 160, font, TextHelper.getNormalizedText(font, skills.get(0).getDescription(),
				220), 64));
		currentSkill = 0;

		BitmapTextureAtlas bta = new BitmapTextureAtlas(128, 128, TextureOptions.DEFAULT);
		TextureRegion tr = BitmapTextureAtlasTextureRegionFactory.createFromAsset(bta, this.getApplicationContext(),
				monster.getImagePath(), 0, 0);
		Sprite sprite = new Sprite(431, 32, tr);
		this.mEngine.getTextureManager().loadTexture(bta);
		scene.attachChild(sprite);

		bta = new BitmapTextureAtlas(32, 512, TextureOptions.DEFAULT);
		tr = BitmapTextureAtlasTextureRegionFactory.createFromAsset(bta, this.getApplicationContext(),
				"gfx/frames/prevMonster.png", 0, 0);
		sprite = new Sprite(0, 0, tr);
		this.mEngine.getTextureManager().loadTexture(bta);
		scene.attachChild(sprite);

		bta = new BitmapTextureAtlas(32, 512, TextureOptions.DEFAULT);
		tr = BitmapTextureAtlasTextureRegionFactory.createFromAsset(bta, this.getApplicationContext(),
				"gfx/frames/nextMonster.png", 0, 0);
		sprite = new Sprite(572, 0, tr);
		this.mEngine.getTextureManager().loadTexture(bta);
		scene.attachChild(sprite);

		bta = new BitmapTextureAtlas(256, 512, TextureOptions.DEFAULT);
		tr = BitmapTextureAtlasTextureRegionFactory.createFromAsset(bta, this.getApplicationContext(),
				"gfx/frames/statFrame.png", 0, 0);
		sprite = new Sprite(32, 0, tr);
		this.mEngine.getTextureManager().loadTexture(bta);
		scene.attachChild(sprite);

		bta = new BitmapTextureAtlas(256, 512, TextureOptions.DEFAULT);
		tr = BitmapTextureAtlasTextureRegionFactory.createFromAsset(bta, this.getApplicationContext(),
				"gfx/frames/skillFrame.png", 0, 0);
		sprite = new Sprite(192, 0, tr);
		this.mEngine.getTextureManager().loadTexture(bta);
		scene.attachChild(sprite);

		bta = new BitmapTextureAtlas(256, 32, TextureOptions.DEFAULT);
		tr = BitmapTextureAtlasTextureRegionFactory.createFromAsset(bta, this.getApplicationContext(),
				"gfx/frames/prevSkill.png", 0, 0);
		sprite = new Sprite(192, 0, tr);
		this.mEngine.getTextureManager().loadTexture(bta);
		scene.attachChild(sprite);
		prevSkillSprite = sprite;

		bta = new BitmapTextureAtlas(256, 32, TextureOptions.DEFAULT);
		tr = BitmapTextureAtlasTextureRegionFactory.createFromAsset(bta, this.getApplicationContext(),
				"gfx/frames/nextSkill.png", 0, 0);
		sprite = new Sprite(192, 328, tr);
		this.mEngine.getTextureManager().loadTexture(bta);
		scene.attachChild(sprite);
		nextSkillSprite = sprite;
		
		bta = new BitmapTextureAtlas(512, 512, TextureOptions.DEFAULT);
		tr = BitmapTextureAtlasTextureRegionFactory.createFromAsset(bta, this.getApplicationContext(),
				"gfx/frames/monsterFrame.png", 0, 0);
		sprite = new Sprite(412, 0, tr);
		this.mEngine.getTextureManager().loadTexture(bta);
		scene.attachChild(sprite);

		for (Text text : textList)
		{
			scene.attachChild(text);
		}
		for (ChangeableText text : skillTextList)
		{
			scene.attachChild(text);
		}
		scene.setVisible(true);
	}
	
	//name, type, cost, power, description
	private void changeSkill(int index)
	{
		Skill skill = skills.get(index);
		
		skillTextList.get(0).setText(skill.getName());
		Log.d("STATUS", "Skill name: " + skill.getName());
		skillTextList.get(1).setText("Type: " + skill.getType().toString());
		skillTextList.get(2).setText("Cost: " + skill.getCost() + "MP");
		skillTextList.get(3).setText("Power: " + skill.getDamage());
		skillTextList.get(4).setText(TextHelper.getNormalizedText(font, skill.getDescription(), 210));
	}
	
	@Override
	public void onResumeGame()
	{
		super.onResumeGame();
		if(music != null && !music.isPlaying())
		{
			music.resume();
		}
	}

	@Override
	protected void onPause()
	{
		super.onPause();
		if (music != null)
		{
			music.pause();
		}
	}
}
