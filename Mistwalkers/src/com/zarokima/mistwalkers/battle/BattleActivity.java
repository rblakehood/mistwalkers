package com.zarokima.mistwalkers.battle;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Random;
import org.anddev.andengine.audio.music.Music;
import org.anddev.andengine.audio.music.MusicFactory;
import org.anddev.andengine.engine.Engine;
import org.anddev.andengine.engine.camera.BoundCamera;
import org.anddev.andengine.engine.options.EngineOptions;
import org.anddev.andengine.engine.options.EngineOptions.ScreenOrientation;
import org.anddev.andengine.engine.options.resolutionpolicy.RatioResolutionPolicy;
import org.anddev.andengine.entity.scene.Scene;
import org.anddev.andengine.entity.util.FPSLogger;
import org.anddev.andengine.ui.activity.BaseGameActivity;
import org.anddev.andengine.util.Debug;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import com.zarokima.mistwalkers.R;
import com.zarokima.mistwalkers.items.Inventory;
import com.zarokima.mistwalkers.items.Item;
import com.zarokima.mistwalkers.monsters.Monster;
import com.zarokima.mistwalkers.monsters.Party;
import com.zarokima.mistwalkers.skills.Skill;
import com.zarokima.mistwalkers.skills.Skill.SkillType;

public class BattleActivity extends BaseGameActivity
{
	protected static boolean isEnemyTurn = false;
	// ===========================================================
	// Camera
	// ===========================================================
	private static int CAMERA_WIDTH;
	private static int CAMERA_HEIGHT;

	// ===========================================================
	// Fields
	// ===========================================================
	protected static TextView fightButton, selectButton, itemButton, fleeButton;
	protected static ImageView enemyView1, enemyView2, enemyView3;
	protected static BoundCamera camera;
	protected static Scene scene;
	protected static Context context;
	protected static Music music;
	protected static Party playerParty;
	protected static Party enemyParty;
	protected static Inventory inventory;
	protected static int itemIndex;
	// protected static Item currItem;
	protected static Skill currSkill;
	protected static Monster enemyMonster1;
	protected static Monster enemyMonster2;
	protected static Monster enemyMonster3;
	protected static Monster currMonster;

	protected static Monster playerMonster1;
	protected static Monster playerMonster2;
	protected static Monster playerMonster3;

	protected static LLObjectNode currNode;

	protected static int numExhaustedEnemyMonsters = 0;
	protected static int numExhaustedPlayerMonsters = 0;

	protected static int SIZE = 6;
	protected static Monster values[] = new Monster[SIZE];

	protected static boolean[] exhaustedEnemyMonsters = new boolean[3];
	protected static boolean[] exhaustedPlayerMonsters = new boolean[3];
	protected static boolean matchOver;
	protected static LinkedUnbndQueue monstersLinkedQueue;

	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		setContentView(R.layout.battlescreen);

		Bundle extras = getIntent().getExtras();
		if (extras == null)
		{
			Log.d("Monsters", "Intent has no extras");
			this.finish();
		}

		try
		{
			playerParty = (Party) extras.getSerializable("PlayerParty");
			enemyParty = (Party) extras.getSerializable("EnemyParty");
			inventory = (Inventory) extras.getSerializable("Inventory");
		}
		catch (Exception e)
		{
			Log.d("Monsters", "Something's wrong with the bundle");
			this.finish();
		}

		// fix for odd crash in Ice Cream Sandwich
		Configuration config = getResources().getConfiguration();
		if (config.locale == null)
			config.locale = Locale.getDefault();
	}

	public void loadDataFromAsset()
	{
		// load images
		try
		{
			// get input stream
			InputStream ims = getAssets().open(enemyMonster1.getImagePath());
			// load image as Drawable
			Drawable d = Drawable.createFromStream(ims, null);
			// set image to ImageView
			enemyView1.setImageDrawable(d);

			ims = getAssets().open(enemyMonster2.getImagePath());
			d = Drawable.createFromStream(ims, null);
			enemyView2.setImageDrawable(d);

			ims = getAssets().open(enemyMonster3.getImagePath());
			d = Drawable.createFromStream(ims, null);
			enemyView3.setImageDrawable(d);
		}
		catch (IOException ex)
		{
			return;
		}
	}

	@Override
	public Engine onLoadEngine()
	{
		DisplayMetrics metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);
		CAMERA_WIDTH = 1028;
		CAMERA_HEIGHT = 780;

		context = getApplicationContext();

		camera = new BoundCamera(0, 0, CAMERA_WIDTH, CAMERA_HEIGHT);
		EngineOptions eo = new EngineOptions(true, ScreenOrientation.LANDSCAPE, new RatioResolutionPolicy(CAMERA_WIDTH,
				CAMERA_HEIGHT), camera);
		eo.getTouchOptions().setRunOnUpdateThread(true);
		eo.setNeedsMusic(true);
		return new Engine(eo);
	}

	@Override
	public void onLoadResources()
	{
		MusicFactory.setAssetBasePath("mfx/");
		try
		{
			music = MusicFactory.createMusicFromAsset(this.mEngine.getMusicManager(), this, "BattleMarch.wav");
			music.setLooping(true);
		}
		catch (final IOException e)
		{
			Debug.e(e);
		}
	}

	@Override
	public Scene onLoadScene()
	{
		this.mEngine.registerUpdateHandler(new FPSLogger());

		music.play();
		createButtonLayout();

		enemyView1 = (ImageView) findViewById(R.id.enemy1View);
		enemyView1.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				enemyMonsterStatDialog(enemyMonster1);
			}
		});

		enemyView2 = (ImageView) findViewById(R.id.enemy2View);
		enemyView2.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				enemyMonsterStatDialog(enemyMonster2);
			}
		});

		enemyView3 = (ImageView) findViewById(R.id.enemy3View);
		enemyView3.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				enemyMonsterStatDialog(enemyMonster3);
			}
		});

		currNode = null;
		currMonster = null;
		matchOver = false;

		playerMonster1 = playerParty.getMonster1();
		playerMonster2 = playerParty.getMonster2();
		playerMonster3 = playerParty.getMonster3();

		TextView t = (TextView) findViewById(R.id.PlayerMonster1Name);
		t.setText(playerMonster1.getName());

		t = (TextView) findViewById(R.id.PlayerMonster2Name);
		t.setText(playerMonster2.getName());

		t = (TextView) findViewById(R.id.PlayerMonster3Name);
		t.setText(playerMonster3.getName());

		monstersLinkedQueue = new LinkedUnbndQueue();

		enemyMonster1 = enemyParty.getMonster1();
		enemyMonster2 = enemyParty.getMonster2();
		enemyMonster3 = enemyParty.getMonster3();

		enemyMonster1.setAI(true);
		enemyMonster2.setAI(true);
		enemyMonster3.setAI(true);

		updatePlayerStats();
		loadDataFromAsset();

		exhaustedEnemyMonsters[0] = false;
		exhaustedEnemyMonsters[1] = false;
		exhaustedEnemyMonsters[2] = false;

		exhaustedPlayerMonsters[0] = false;
		exhaustedPlayerMonsters[1] = false;
		exhaustedPlayerMonsters[2] = false;

		values[0] = playerMonster1;
		values[1] = playerMonster2;
		values[2] = playerMonster3;
		values[3] = enemyMonster1;
		values[4] = enemyMonster2;
		values[5] = enemyMonster3;

		bubbleSort();

		monstersLinkedQueue.enqueue(values[0]);
		monstersLinkedQueue.enqueue(values[1]);
		monstersLinkedQueue.enqueue(values[2]);
		monstersLinkedQueue.enqueue(values[3]);
		monstersLinkedQueue.enqueue(values[4]);
		monstersLinkedQueue.enqueue(values[5]);

		currNode = monstersLinkedQueue.front;

		monsterTurn();

		return scene;
	}

	@Override
	public void onLoadComplete()
	{

	}

	private void createButtonLayout()
	{
		fightButton = (TextView) findViewById(R.id.fight_btn);
		fightButton.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				fightClicked();
			}
		});

		selectButton = (TextView) findViewById(R.id.select_btn);
		selectButton.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				selectClicked();
			}
		});

		itemButton = (TextView) findViewById(R.id.item_btn);
		itemButton.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				itemClicked();
			}
		});

		fleeButton = (TextView) findViewById(R.id.flee_btn);
		fleeButton.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				fleeClicked();
			}
		});
	}

	static public void swap(int index1, int index2)
	// Precondition: index1 and index2 are >= 0 and < SIZE.
	//
	// Swaps the integers at locations index1 and index2 of the values array.
	{
		Monster temp = values[index1];
		values[index1] = values[index2];
		values[index2] = temp;
	}

	static void bubbleUp(int startIndex, int endIndex)
	// Switches adjacent pairs that are out of order
	// between values[startIndex]..values[endIndex]
	// beginning at values[endIndex].
	{
		for (int index = endIndex; index > startIndex; index--)
			if (values[index].getAgility() > values[index - 1].getAgility())
				swap(index, index - 1);
	}

	static void bubbleSort()
	// Sorts the values array using the bubble sort algorithm.
	{
		int current = 0;

		while (current < (SIZE - 1))
		{
			bubbleUp(current, SIZE - 1);
			current++;
		}
	}

	private void updatePlayerStats()
	{
		TextView pMonsterHealthView, pMonsterMagicView;

		pMonsterHealthView = (TextView) findViewById(R.id.playerHealth1);
		pMonsterHealthView.setText("HP: " + playerMonster1.getCurrentHP() + "/" + playerMonster1.getMaxHP());

		pMonsterHealthView = (TextView) findViewById(R.id.playerHealth2);
		pMonsterHealthView.setText("HP: " + playerMonster2.getCurrentHP() + "/" + playerMonster2.getMaxHP());

		pMonsterHealthView = (TextView) findViewById(R.id.playerHealth3);
		pMonsterHealthView.setText("HP: " + playerMonster3.getCurrentHP() + "/" + playerMonster3.getMaxHP());

		pMonsterMagicView = (TextView) findViewById(R.id.playerMagic1);
		pMonsterMagicView.setText("MP: " + playerMonster1.getCurrentMP() + "/" + playerMonster1.getMaxMP());

		pMonsterMagicView = (TextView) findViewById(R.id.playerMagic2);
		pMonsterMagicView.setText("MP: " + playerMonster2.getCurrentMP() + "/" + playerMonster2.getMaxMP());

		pMonsterMagicView = (TextView) findViewById(R.id.playerMagic3);
		pMonsterMagicView.setText("MP: " + playerMonster3.getCurrentMP() + "/" + playerMonster3.getMaxMP());
	}

	private void monsterTurn()
	{

		boolean foundAliveMonster = false;

		while (!foundAliveMonster)
		{
			currMonster = (Monster) currNode.getInfo();

			if (currMonster.isAlive())
			{
				foundAliveMonster = true;
			}
			else
			{
				if (currNode == monstersLinkedQueue.rear)
				{
					currNode = monstersLinkedQueue.front;
				}
				else
					currNode = currNode.getLink();
			}
		}

		if (currMonster.isAI())
		{
			turnAI(currMonster);
			updatePlayerStats();
			if (currNode.equals(monstersLinkedQueue.rear))
			{
				currNode = monstersLinkedQueue.front;
			}
			else
				currNode = currNode.getLink();
		}
		else
		{
			beginTurnDialog(currMonster.getName());
		}
	}

	private void turnAI(Monster currMonster)
	{
		final Monster AIMonster = currMonster;
		Monster currPlayerMonster = null;

		int currAttack = 0, playerIndex = 0;

		boolean aliveMonster = false;

		while (!aliveMonster)
		{
			Random gen = new Random();
			playerIndex = gen.nextInt(3);

			switch (playerIndex)
			{
				case 0:
					if (playerMonster1.isAlive())
					{
						aliveMonster = true;
						currPlayerMonster = playerMonster1;
					}
					break;
				case 1:
					if (playerMonster2.isAlive())
					{
						aliveMonster = true;
						currPlayerMonster = playerMonster2;
					}
					break;
				case 2:
					if (playerMonster3.isAlive())
					{
						aliveMonster = true;
						currPlayerMonster = playerMonster3;
					}
					break;
			}
		}
		AIMonster.getSkills().get(0).applyDamage(AIMonster, currPlayerMonster);
		float ratio = AIMonster.getPhysicalAttack() / (float) currPlayerMonster.getPhysicalDefense();
		currAttack = (int) (AIMonster.getPhysicalAttack() * ratio);

		if (currPlayerMonster.getCurrentHP() <= 0)
		{
			numExhaustedPlayerMonsters++;
			exhaustedPlayerMonsters[playerIndex] = true;
			if (numExhaustedPlayerMonsters == 3)
			{
				loseDialog(AIMonster.getName(), currPlayerMonster.getName(), currAttack);
				matchOver = true;
			}
			else
			{
				exhaustedDialog(AIMonster.getName(), currPlayerMonster.getName(), currAttack);
			}

		}
		else
		{
			attackDialog(AIMonster.getName(), currPlayerMonster.getName(), currAttack);
		}
		updatePlayerStats();
	}

	private void fightClicked()
	{
		selectEnemyDialog(true);
		if (currNode.equals(monstersLinkedQueue.rear))
		{
			currNode = monstersLinkedQueue.front;
		}
		else
			currNode = currNode.getLink();
	}

	private void selectClicked()
	{
		selectMonsterSkill();
	}

	private void itemClicked()
	{
		selectInventoryItem();
	}

	private void fleeClicked()
	{
		finish();
	}

	private void selectEnemyDialog(final boolean isBasicAttack)
	{
		String[] monsterList = new String[3];
		monsterList[0] = enemyMonster1.getName();
		monsterList[1] = enemyMonster2.getName();
		monsterList[2] = enemyMonster3.getName();

		AlertDialog.Builder dialog = new AlertDialog.Builder(this);
		dialog.setTitle("Select a monster.");
		dialog.setItems(monsterList, new DialogInterface.OnClickListener()
		{
			public void onClick(DialogInterface dialog, int which)
			{
				dialog.dismiss();

				int currAttack = 0;
				Monster selectedEnemyMonster = null;
				ImageView view = null;

				if (!exhaustedEnemyMonsters[which])
				{
					switch (which)
					{
						case 0:
							selectedEnemyMonster = enemyMonster1;
							view = (ImageView) findViewById(R.id.enemy1View);
							break;
						case 1:
							selectedEnemyMonster = enemyMonster2;
							view = (ImageView) findViewById(R.id.enemy2View);
							break;
						case 2:
							selectedEnemyMonster = enemyMonster3;
							view = (ImageView) findViewById(R.id.enemy3View);
							break;
					}

					if (isBasicAttack)
					{
						currMonster.getSkills().get(0).applyDamage(currMonster, selectedEnemyMonster);
						float ratio = currMonster.getPhysicalAttack()
								/ (float) selectedEnemyMonster.getPhysicalDefense();
						currAttack = (int) (currMonster.getPhysicalAttack() * ratio);
					}
					else
					{
						int attackPoints = currSkill.getType() == SkillType.PHYSICAL ? currMonster.getPhysicalAttack()
								: currMonster.getMagicalAttack();
						int defensePoints = currSkill.getType() == SkillType.PHYSICAL ? selectedEnemyMonster
								.getPhysicalDefense() : selectedEnemyMonster.getPhysicalDefense();
						float ratio = attackPoints / (float) defensePoints;

						currSkill.useSkill(currMonster, selectedEnemyMonster);
						currAttack = Math.round(attackPoints + currSkill.getDamage() * ratio);
					}

					if (selectedEnemyMonster.getCurrentHP() <= 0)
					{
						view.setVisibility(View.INVISIBLE);
						numExhaustedEnemyMonsters++;

						switch (which)
						{
							case 0:
								exhaustedEnemyMonsters[0] = true;
								break;
							case 1:
								exhaustedEnemyMonsters[1] = true;
								break;
							case 2:
								exhaustedEnemyMonsters[2] = true;
								break;
							default:
								break;
						}

						if (numExhaustedEnemyMonsters == 3)
						{
							victoryDialog(currMonster.getName(), selectedEnemyMonster.getName(), currAttack);
							numExhaustedEnemyMonsters = 0;
							matchOver = true;
						}
						else
						{
							exhaustedDialog(currMonster.getName(), selectedEnemyMonster.getName(), currAttack);
						}
					}
					else
					{
						attackDialog(currMonster.getName(), selectedEnemyMonster.getName(), currAttack);
					}
					updatePlayerStats();
				}
				else
				{
					invalidMonsterDialog(currMonster);
				}
			}
		});
		dialog.show();
	}

	private void useSkillOnAlly()
	{
		String[] monsterList = new String[3];
		monsterList[0] = playerMonster1.getName();
		monsterList[1] = playerMonster2.getName();
		monsterList[2] = playerMonster3.getName();

		AlertDialog.Builder dialog = new AlertDialog.Builder(this);
		dialog.setTitle("Select a ally.");
		dialog.setItems(monsterList, new DialogInterface.OnClickListener()
		{
			public void onClick(DialogInterface dialog, int which)
			{
				dialog.dismiss();

				int amount = 0;
				Monster selectedAlly = null;

				if (!exhaustedPlayerMonsters[which])
				{
					switch (which)
					{
						case 0:
							selectedAlly = playerMonster1;
							break;
						case 1:
							selectedAlly = playerMonster2;
							break;
						case 2:
							selectedAlly = playerMonster3;
							break;
					}

					currSkill.useSkill(currMonster, selectedAlly);
					amount = currSkill.getDamage() + currMonster.getMagicalAttack();
					healAllyDialog(currMonster.getName(), selectedAlly.getName(), amount);
					updatePlayerStats();
				}
				else
				{
					useSkillOnAlly();
				}
			}
		});
		dialog.show();
	}

	private void useItemOnAlly()
	{
		String[] monsterList = new String[3];
		monsterList[0] = playerMonster1.getName();
		monsterList[1] = playerMonster2.getName();
		monsterList[2] = playerMonster3.getName();

		AlertDialog.Builder dialog = new AlertDialog.Builder(this);
		dialog.setTitle("Select a ally.");
		dialog.setItems(monsterList, new DialogInterface.OnClickListener()
		{
			public void onClick(DialogInterface dialog, int which)
			{
				dialog.dismiss();

				// String description = currItem.getDescription();
				// Monster selectedAlly = null;

				if (!exhaustedPlayerMonsters[which])
				{
					switch (which)
					{
						case 0:
							inventory.useItem(itemIndex, playerMonster1);
							itemDialog(currMonster.getName(), playerMonster1.getName());
							break;
						case 1:
							inventory.useItem(itemIndex, playerMonster2);
							itemDialog(currMonster.getName(), playerMonster2.getName());
							break;
						case 2:
							inventory.useItem(itemIndex, playerMonster3);
							itemDialog(currMonster.getName(), playerMonster3.getName());
							break;
					}
					updatePlayerStats();
				}
				else
				{
					useItemOnAlly();
				}
			}
		});
		dialog.show();
	}

	private void selectMonsterSkill()
	{
		final ArrayList<Skill> skillList = currMonster.getSkills();

		String[] skillStringList = new String[skillList.size()];
		int index = 0;

		for (Skill skill : skillList)
		{
			skillStringList[index] = skill.getName();
			index++;
		}

		AlertDialog.Builder dialog = new AlertDialog.Builder(this);
		dialog.setTitle("Select a skill.");
		dialog.setItems(skillStringList, new DialogInterface.OnClickListener()
		{
			public void onClick(DialogInterface dialog, int which)
			{
				dialog.dismiss();

				currSkill = skillList.get(which);

				if (currSkill.getType() == Skill.SkillType.HEALING)
				{
					useSkillOnAlly();
				}
				else
					selectEnemyDialog(false);

				if (currNode.equals(monstersLinkedQueue.rear))
				{
					currNode = monstersLinkedQueue.front;
				}
				else
					currNode = currNode.getLink();
			}
		});
		dialog.show();
	}

	private void selectInventoryItem()
	{
		final ArrayList<Item> itemList = inventory.getItems();

		String[] itemStringList = new String[itemList.size()];
		int index = 0;

		for (Item item : itemList)
		{
			itemStringList[index] = item.getName();
			index++;
		}

		AlertDialog.Builder dialog = new AlertDialog.Builder(this);
		dialog.setTitle("Select a item.");
		dialog.setItems(itemStringList, new DialogInterface.OnClickListener()
		{
			public void onClick(DialogInterface dialog, int which)
			{
				dialog.dismiss();
				// Monster[] targets = new Monster[2];

				itemIndex = which;

				useItemOnAlly();
				/*
				 * if(currItem.isConsumeable()) { targets[0] = useSkillOnAlly(); if(currItem.isMultiTarget()) {
				 * useSkillOnAlly(); targets[1] = useSkillOnAlly(); } inventory.useItem(which, targets); } else
				 * //selectEnemyDialog(false);
				 */
				if (currNode.equals(monstersLinkedQueue.rear))
				{
					currNode = monstersLinkedQueue.front;
				}
				else
					currNode = currNode.getLink();
			}
		});
		dialog.show();

	}

	private void beginTurnDialog(String playerMonster)
	{
		AlertDialog begin = new AlertDialog.Builder(this).create();
		begin.setMessage("Select your attack for " + playerMonster + ":");
		begin.setCanceledOnTouchOutside(true);
		begin.show();
	}

	private void exhaustedDialog(String playerMonsterName, String enemyMonsterName, int currAttack)
	{
		AlertDialog alert = new AlertDialog.Builder(this).create();
		alert.setMessage(playerMonsterName + " attacks " + enemyMonsterName + " for " + currAttack + " points! "
				+ enemyMonsterName + " is unable to battle anymore!");
		alert.setCanceledOnTouchOutside(true);
		alert.setOnDismissListener(new OnDismissListener()
		{

			@Override
			public void onDismiss(DialogInterface arg0)
			{
				if (!matchOver)
				{
					monsterTurn();
				}
			}

		});
		alert.show();
	}

	private void attackDialog(String enemyMonsterName, String playerMonsterName, int currAttack)
	{
		AlertDialog alert = new AlertDialog.Builder(this).create();
		alert.setMessage(enemyMonsterName + " attacks " + playerMonsterName + " for " + currAttack + " points!");
		alert.setCanceledOnTouchOutside(true);
		alert.setOnDismissListener(new OnDismissListener()
		{

			@Override
			public void onDismiss(DialogInterface arg0)
			{
				if (!matchOver)
				{
					monsterTurn();
				}
			}

		});
		alert.show();
	}

	private void invalidMonsterDialog(final Monster currMonster2)
	{
		AlertDialog alert = new AlertDialog.Builder(this).create();
		alert.setMessage("Come on man, select another monster to attack...");
		alert.setCanceledOnTouchOutside(true);
		alert.setOnDismissListener(new DialogInterface.OnDismissListener()
		{

			@Override
			public void onDismiss(DialogInterface dialog)
			{

			}
		});
		alert.show();
	}

	private void victoryDialog(String attackMonsterName, String targetMonsterName, int currAttack)
	{
		AlertDialog alert = new AlertDialog.Builder(this).create();
		alert.setMessage(attackMonsterName + " attacks " + targetMonsterName + " for " + currAttack + " points! "
				+ targetMonsterName + " is unable to battle anymore!" + " You have defeated all enemy monsters! ");
		alert.setCanceledOnTouchOutside(true);
		alert.setOnDismissListener(new DialogInterface.OnDismissListener()
		{
			@Override
			public void onDismiss(DialogInterface dialog)
			{
				finish();

			}
		});
		alert.show();
	}

	private void loseDialog(String attackMonsterName, String targetMonsterName, int currAttack)
	{
		AlertDialog alert = new AlertDialog.Builder(this).create();
		alert.setMessage(attackMonsterName + " attacks " + targetMonsterName + " for " + currAttack + " points! "
				+ targetMonsterName + " is unable to battle anymore!" + " Games Over!!!! ");
		alert.setCanceledOnTouchOutside(true);
		alert.setOnDismissListener(new DialogInterface.OnDismissListener()
		{
			@Override
			public void onDismiss(DialogInterface dialog)
			{
				finish();
			}
		});
		alert.show();
	}

	private void itemDialog(String user, String target)
	{
		AlertDialog alert = new AlertDialog.Builder(this).create();
		alert.setMessage(user + " heals " + target);
		alert.setCanceledOnTouchOutside(true);
		alert.setOnDismissListener(new OnDismissListener()
		{

			@Override
			public void onDismiss(DialogInterface arg0)
			{
				if (!matchOver)
				{
					monsterTurn();
				}
			}
		});
		alert.show();
	}

	private void healAllyDialog(String user, String target, int amount)
	{
		AlertDialog alert = new AlertDialog.Builder(this).create();
		alert.setMessage(user + " heals " + target + " for " + amount);
		alert.setCanceledOnTouchOutside(true);
		alert.setOnDismissListener(new OnDismissListener()
		{

			@Override
			public void onDismiss(DialogInterface arg0)
			{
				if (!matchOver)
				{
					monsterTurn();
				}
			}
		});
		alert.show();
	}

	private void enemyMonsterStatDialog(Monster enemyMonster)
	{
		AlertDialog status = new AlertDialog.Builder(this).create();
		status.setMessage(enemyMonster.getName() + "\n" + "HP: " + enemyMonster.getCurrentHP() + "/"
				+ enemyMonster.getMaxHP() + "\n" + "MP: " + enemyMonster.getCurrentMP() + "/" + enemyMonster.getMaxMP());
		status.setCanceledOnTouchOutside(true);
		status.show();
	}
}
