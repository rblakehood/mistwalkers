package com.zarokima.mistwalkers.battle;

public class QueueUnderflowException extends RuntimeException
{
  /**
	 * 
	 */
	private static final long serialVersionUID = -378407998425681410L;

public QueueUnderflowException()
  {
    super();
  }

  public QueueUnderflowException(String message)
  {
    super(message);
  }
}