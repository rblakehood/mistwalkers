package com.zarokima.mistwalkers.battle;

public interface UnboundedQueueInterface extends QueueInterface

{
  void enqueue(Object element);
  // Adds element to the rear of this queue.
}
