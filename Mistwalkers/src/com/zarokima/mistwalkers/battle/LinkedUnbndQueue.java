package com.zarokima.mistwalkers.battle;


public class LinkedUnbndQueue implements UnboundedQueueInterface
{
  protected LLObjectNode front;   // reference to the front of this queue
  protected LLObjectNode rear;    // reference to the rear of this queue

  public LinkedUnbndQueue()
  {
    front = null;
    rear = null;
  }

  public void enqueue(Object element)
  // Adds element to the rear of this queue.
  { 
    LLObjectNode newNode = new LLObjectNode(element);
    if (rear == null)
      front = newNode;
    else
      rear.setLink(newNode);
    rear = newNode;
  }     

  public Object dequeue()
  // Throws QueueUnderflowException if this queue is empty;
  // otherwise, removes front element from this queue and returns it.
  {
    if (isEmpty())
      throw new QueueUnderflowException("Dequeue attempted on empty queue.");
    else
    {
      Object element;
      element = front.getInfo();
      front = front.getLink();
      if (front == null)
        rear = null;

      return element;
    }
  }

  public boolean isEmpty()
  // Returns true if this queue is empty; otherwise, returns false.
  {              
    if (front == null) 
      return true;
    else
      return false;
  }
}

