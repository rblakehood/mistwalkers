package com.zarokima.mistwalkers.skills;

public class TestPhysicalDamageSkill extends Skill
{

	public TestPhysicalDamageSkill()
	{
		super(SkillType.PHYSICAL, 50, 3);
		description = "Deals physical damage to the target.";
	}

}
