package com.zarokima.mistwalkers.skills;

import com.zarokima.mistwalkers.monsters.Monster;

public class BasicAttack extends Skill
{
	public BasicAttack()
	{
		super(SkillType.PHYSICAL, 1, 0);
		description = "A simple attack that does nothing special.";
	}

	//overridden because basic attacks will be based purely on user's attack rather
	//the skill's damage variable
	@Override
	public void applyDamage(Monster user, Monster target)
	{
		int attackPoints = user.getPhysicalAttack();
		int defensePoints = target.getPhysicalDefense();
		
		float ratio = attackPoints / (float) defensePoints;
		
		target.inflictDamage(Math.round(attackPoints * ratio));
	}
}
