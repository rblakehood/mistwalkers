package com.zarokima.mistwalkers.skills;

public class TestMagicalDamageSkill extends Skill
{
	public TestMagicalDamageSkill()
	{
		super(SkillType.MAGICAL, 50, 3);
		description = "Deals magical damage to the target.";
	}
}
