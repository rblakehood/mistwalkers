package com.zarokima.mistwalkers.skills;

public class TestHealingSkill extends Skill
{
	public TestHealingSkill()
	{
		super(SkillType.HEALING, 50, 3);
		description = "Heals the target.";
	}
}
