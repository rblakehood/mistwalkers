package com.zarokima.mistwalkers.skills;

import java.io.Serializable;
import android.util.Log;
import com.zarokima.mistwalkers.monsters.Monster;

//this is the base class for skills
//all actual skills should extend this class
public abstract class Skill implements Serializable
{
	public enum SkillType
	{
		PHYSICAL, MAGICAL, HEALING
	};

	protected int damage;
	protected int cost;
	protected SkillType type;
	protected String name;
	protected String description;

	public Skill(SkillType t, int d, int c)
	{
		type = t;
		damage = d;
		cost = c;
		name = this.getClass().getSimpleName();
		Log.d("SKILL", name);
		description = "No description";
	}

	public final void useSkill(Monster user, Monster target)
	{
		if (cost > user.getCurrentMP())
		{
			return;
		}

		user.setCurrentMP(user.getCurrentMP() - cost);

		applyDamage(user, target);
		applyEffect(user, target);
	}

	public void applyDamage(Monster user, Monster target)
	{
		if (damage == 0)
			return;

		// healing is handled special because we don't want them to resist
		// something that's good
		if (type == SkillType.HEALING)
		{
			int healAmount = damage + user.getMagicalAttack();
			healAmount = -healAmount; // value must be negative to heal
			target.inflictDamage(healAmount);
			return;
		}

		// damage is determined by a ratio rather than just subtracting defense from attack
		// because a higher defense would result in being healed by a damage skill
		// so this just reduces/increases the damage based on relative power
		int attackPoints = type == SkillType.PHYSICAL ? user.getPhysicalAttack() : user.getMagicalAttack();
		int defensePoints = type == SkillType.PHYSICAL ? target.getPhysicalDefense() : target.getPhysicalDefense();
		float ratio = attackPoints / (float) defensePoints;

		target.inflictDamage(Math.round((attackPoints + damage) * ratio));
	}

	protected void applyEffect(Monster user, Monster target)
	{
		// somehow do a status effect
	}

	// *********
	// getters
	// *********
	public int getDamage()
	{
		return damage;
	}

	public int getCost()
	{
		return cost;
	}

	public SkillType getType()
	{
		return type;
	}

	public String getName()
	{
		return name;
	}

	public String getDescription()
	{
		return description;
	}
}
